from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.views.decorators.csrf import csrf_exempt

from app.internal.app import get_api
from app.internal.bot.views import TelegramBotWebhookView

api = get_api()


urlpatterns = (
    [
        path("admin/", admin.site.urls),
        path("api/", api.urls),
        path(f"{settings.TOKEN}/", csrf_exempt(TelegramBotWebhookView.as_view())),
    ]
    + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
)
