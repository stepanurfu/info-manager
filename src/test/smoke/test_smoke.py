import pytest
from django.contrib.auth import get_user_model
from django.urls import reverse_lazy

from app.internal.authentication.domain.services import AuthService
from app.internal.bot.bot import telegram_bot_webhook
from app.internal.person.db.models import Person

User = get_user_model()


def test_bot_webhook():
    telegram_bot_webhook.process({})


@pytest.mark.django_db
def test_db_access():
    User.objects.create_user("1" * 11, "lennon", "johnpassword")
    assert User.objects.count() == 1


@pytest.mark.django_db
def test_view(client, test_person, test_auth_service: AuthService):
    jwt_tokens = test_auth_service.generate_jwt_tokens(test_person)
    url = reverse_lazy("api-1.0.0:my_info")
    auth_headers = {
        "HTTP_AUTHORIZATION": "Bearer " + jwt_tokens.access_token,
    }
    response = client.get(url, **auth_headers)
    assert response.status_code == 200
