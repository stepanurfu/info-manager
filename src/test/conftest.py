import pytest

from app.internal.authentication.db.repositories import IssuedTokenRepository
from app.internal.authentication.domain.services import AuthService
from app.internal.bank_account.db.models import BankAccount
from app.internal.bank_account.db.repositories import BankAccountRepository
from app.internal.bank_card.db.models import BankCard
from app.internal.bank_card.db.repositories import BankCardRepository
from app.internal.bank_transaction.db.repositories import BankTransactionRepository
from app.internal.bank_transaction.domain.services import BankTransactionService
from app.internal.person.db.models import Person
from app.internal.person.db.repositories import PersonRepository
from app.internal.person.domain.services import PersonService


@pytest.fixture(scope="function")
def test_person(id=123, name="Name"):
    return Person.objects.create(pk=id, telegram_chat_id=id, name=name, phone="1" * 11)


@pytest.fixture(scope="function")
def test_person_with_account_and_card(id=123, name="Name1", acc_number="1" * 20, card_number="2" * 16):
    person = Person.objects.create(pk=id, telegram_chat_id=id, name=name, phone="11111111111")
    account = BankAccount.objects.create(pk=100, number=acc_number)
    card = BankCard.objects.create(pk=100, number=card_number, bank_account=account, code=123)
    account.customers.add(person)
    account.save()
    return person, account, card


@pytest.fixture(scope="function")
def test_person_with_account_and_card_2(id=124, name="Name2", acc_number="3" * 20, card_number="4" * 16):
    person = Person.objects.create(pk=id, telegram_chat_id=id, name=name, phone="11111111112")
    account = BankAccount.objects.create(pk=101, number=acc_number)
    card = BankCard.objects.create(pk=101, number=card_number, bank_account=account, code=123)
    account.customers.add(person)
    account.save()
    return person, account, card


@pytest.fixture(scope="function")
def test_person_repo():
    return PersonRepository()


@pytest.fixture(scope="function")
def test_person_service():
    repo = PersonRepository()
    return PersonService(repo)


@pytest.fixture(scope="function")
def test_bank_card_repo():
    return BankCardRepository()


@pytest.fixture(scope="function")
def test_bank_account_repo():
    return BankAccountRepository()


@pytest.fixture(scope="function")
def test_bank_transaction_repo():
    return BankTransactionRepository()


@pytest.fixture(scope="function")
def test_bank_transaction_service():
    bank_transaction_repo = BankTransactionRepository()
    bank_account_repo = BankAccountRepository()
    bank_card_repo = BankCardRepository()
    return BankTransactionService(
        bank_transaction_repo=bank_transaction_repo, bank_account_repo=bank_account_repo, bank_card_repo=bank_card_repo
    )


@pytest.fixture(scope="function")
def test_auth_service():
    issued_token_repo = IssuedTokenRepository()
    return AuthService(issued_token_repo=issued_token_repo)
