from unittest.mock import PropertyMock

import pytest

from app.internal.bot.base_handlers import BotState
from app.internal.bot.handlers.favorite_users_list_handlers import (
    add_to_favorite_users_cmd,
    add_to_favorite_users_msg,
    remove_from_favorite_users_list_cmd,
    remove_from_favorite_users_list_msg,
    show_favorite_users_list_command,
)
from app.internal.bot.response_messages import PersonInfoResponseFormats
from app.internal.person.db.models import Person


class TestBotFavoriteUsersListHandlers:
    @pytest.mark.django_db
    def test_add_to_favorite_users_cmd_when_no_phone(self, test_telegram_update):
        next_state = add_to_favorite_users_cmd(test_telegram_update, None)
        assert next_state != BotState.FAVORITE_LIST
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED
        )

    @pytest.mark.django_db
    def test_add_to_favorite_users_cmd_when_have_phone(self, test_telegram_update, test_person):
        next_state = add_to_favorite_users_cmd(test_telegram_update, None)
        assert next_state == BotState.FAVORITE_LIST
        test_telegram_update.message.reply_text.assert_called_once_with(PersonInfoResponseFormats.ENTER_USER_ID)

    @pytest.mark.django_db
    def test_add_to_favorite_users_msg_when_user_not_exist(self, test_telegram_update, test_person):
        type(test_telegram_update.message).text = PropertyMock(return_value="66666")
        add_to_favorite_users_msg(test_telegram_update, None)
        test_telegram_update.message.reply_text.assert_called_once_with(PersonInfoResponseFormats.NO_USER_WITH_ID)

    @pytest.mark.django_db
    def test_add_to_favorite_users_msg(self, test_telegram_update, test_person):
        person = Person.objects.create(telegram_chat_id=321, name="Johann")
        type(test_telegram_update.message).text = PropertyMock(return_value=str(person.telegram_chat_id))
        add_to_favorite_users_msg(test_telegram_update, None)
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.FAVORITE_USERS_LIST_ADD_SUCCESS
        )

        add_to_favorite_users_msg(test_telegram_update, None)
        test_telegram_update.message.reply_text.assert_called_with(
            PersonInfoResponseFormats.FAVORITE_USERS_LIST_ADD_FAIL
        )

    @pytest.mark.django_db
    def test_remove_from_favorite_users_list_cmd_when_no_phone(self, test_telegram_update):
        next_state = remove_from_favorite_users_list_cmd(test_telegram_update, None)
        assert next_state != BotState.FAVORITE_LIST
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED
        )

    @pytest.mark.django_db
    def test_remove_from_favorite_users_list_cmd_when_have_phone(self, test_telegram_update, test_person):
        next_state = remove_from_favorite_users_list_cmd(test_telegram_update, None)
        assert next_state == BotState.FAVORITE_LIST
        test_telegram_update.message.reply_text.assert_called_once_with(PersonInfoResponseFormats.ENTER_USER_ID)

    @pytest.mark.django_db
    def test_remove_from_favorite_users_list_msg_when_user_not_exist(self, test_telegram_update, test_person):
        type(test_telegram_update.message).text = PropertyMock(return_value="66666")
        remove_from_favorite_users_list_msg(test_telegram_update, None)
        test_telegram_update.message.reply_text.assert_called_once_with(PersonInfoResponseFormats.NO_USER_WITH_ID)

    @pytest.mark.django_db
    def test_remove_from_favorite_users_list_msg(self, test_telegram_update, test_person):
        person = Person.objects.create(telegram_chat_id=321, name="Johann")
        test_person.favorite_users_list.add(person)
        test_person.save()
        type(test_telegram_update.message).text = PropertyMock(return_value=str(person.telegram_chat_id))
        remove_from_favorite_users_list_msg(test_telegram_update, None)
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.FAVORITE_USERS_LIST_REMOVE_SUCCESS
        )

        remove_from_favorite_users_list_msg(test_telegram_update, None)
        test_telegram_update.message.reply_text.assert_called_with(
            PersonInfoResponseFormats.FAVORITE_USERS_LIST_REMOVE_FAIL
        )

    @pytest.mark.django_db
    def test_show_favorite_users_list_command_when_have_items(self, test_telegram_update, test_person):
        person = Person.objects.create(telegram_chat_id=321, name="Johann")
        test_person.favorite_users_list.add(person)
        test_person.save()
        show_favorite_users_list_command(test_telegram_update, None)
        call_args = test_telegram_update.message.reply_text.call_args.args
        assert len(call_args) == 1
        assert person.name in call_args[0]

    @pytest.mark.django_db
    def test_show_favorite_users_list_command_when_empty(self, test_telegram_update, test_person):
        show_favorite_users_list_command(test_telegram_update, None)
        call_args = test_telegram_update.message.reply_text.call_args.args
        assert len(call_args) == 1
        assert "Empty" in call_args[0]
