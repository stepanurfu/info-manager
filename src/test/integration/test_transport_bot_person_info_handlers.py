from unittest.mock import PropertyMock, call

import pytest
from django.contrib.auth.hashers import check_password

from app.internal.bot.base_handlers import BotState
from app.internal.bot.handlers.person_info_handlers import (
    date_of_birth,
    fill_interactive,
    help,
    me,
    name,
    phone,
    set_date_of_birth_cmd,
    set_date_of_birth_msg,
    set_name_cmd,
    set_name_msg,
    set_password_cmd,
    set_password_msg,
    set_phone_cmd,
    set_phone_msg,
    skip_date_of_birth,
    skip_name,
    skip_phone,
    start,
)
from app.internal.bot.response_messages import PersonInfoResponseFormats
from app.internal.bot.utils import build_fill_prompt_message
from app.internal.person.db.models import Person


class TestBotPersonInfoHandlers:
    @pytest.mark.django_db
    def test_start(self, test_telegram_update):
        start(test_telegram_update, None)
        test_telegram_update.message.reply_text.assert_called_once_with(PersonInfoResponseFormats.START)
        assert not Person.objects.filter(telegram_chat_id=test_telegram_update.message.chat_id).exists()

    @pytest.mark.django_db
    def test_fill_interactive(self, test_telegram_update):
        next_state = fill_interactive(test_telegram_update, None)
        assert next_state == BotState.PHONE
        test_telegram_update.message.reply_text.assert_has_calls(
            [call(PersonInfoResponseFormats.FILL_INTERACTIVE), call(build_fill_prompt_message("Phone"))]
        )
        assert not Person.objects.filter(telegram_chat_id=test_telegram_update.message.chat_id).exists()

    @pytest.mark.django_db
    def test_phone_when_wrong_phone(self, test_telegram_update):
        type(test_telegram_update.message).text = PropertyMock(return_value="123")
        next_state = phone(test_telegram_update, None)
        assert next_state == BotState.PHONE
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.PHONE_INVALID_FORMAT_TRY_AGAIN
        )

    @pytest.mark.django_db
    def test_phone_when_correct_phone(self, test_telegram_update):
        new_phone = "11111111111"
        type(test_telegram_update.message).text = PropertyMock(return_value=new_phone)
        next_state = phone(test_telegram_update, None)
        assert next_state == BotState.NAME
        test_telegram_update.message.reply_text.assert_called_once()
        person = Person.objects.get(telegram_chat_id=test_telegram_update.message.chat_id)
        assert person.phone == new_phone

    @pytest.mark.django_db
    def test_phone_when_such_phone_exists(self, test_telegram_update):
        new_phone = "11111111111"
        Person.objects.create(telegram_chat_id=333, phone=new_phone)
        type(test_telegram_update.message).text = PropertyMock(return_value=new_phone)
        next_state = phone(test_telegram_update, None)
        assert next_state == BotState.PHONE
        test_telegram_update.message.reply_text.assert_called_once()
        assert not Person.objects.filter(telegram_chat_id=test_telegram_update.message.chat_id).exists()
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.PHONE_ALREADY_EXISTS_TRY_AGAIN
        )

    @pytest.mark.django_db
    def test_skip_phone_when_phone_set(self, test_telegram_update):
        Person.objects.create(telegram_chat_id=test_telegram_update.message.chat_id, phone="11111111111")
        next_state = skip_phone(test_telegram_update, None)
        assert next_state == BotState.NAME

    @pytest.mark.django_db
    def test_skip_phone_when_no_phone(self, test_telegram_update):
        next_state = skip_phone(test_telegram_update, None)
        assert next_state == BotState.PHONE
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.CANNOT_SKIP_MANDATORY_FIELD
        )

    @pytest.mark.django_db
    def test_name(self, test_telegram_update_with_created_person):
        new_name = "John"
        type(test_telegram_update_with_created_person.message).text = PropertyMock(return_value=new_name)
        next_state = name(test_telegram_update_with_created_person, None)
        assert next_state == BotState.DATE_OF_BIRTH
        person = Person.objects.get(telegram_chat_id=test_telegram_update_with_created_person.message.chat_id)
        assert person.name == new_name

    @pytest.mark.django_db
    def test_skip_name(self, test_telegram_update_with_created_person):
        next_state = skip_name(test_telegram_update_with_created_person, None)
        assert next_state == BotState.DATE_OF_BIRTH

    @pytest.mark.django_db
    def test_date_of_birth_when_wrong_format(self, test_telegram_update_with_created_person):
        type(test_telegram_update_with_created_person.message).text = PropertyMock(return_value="abc")
        next_state = date_of_birth(test_telegram_update_with_created_person, None)
        assert next_state == BotState.DATE_OF_BIRTH
        test_telegram_update_with_created_person.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.DATE_OF_BIRTH_INVALID_FORMAT_TRY_AGAIN
        )

    @pytest.mark.django_db
    def test_date_of_birth_when_correct_format(self, test_telegram_update_with_created_person):
        type(test_telegram_update_with_created_person.message).text = PropertyMock(return_value="2022-01-02")
        date_of_birth(test_telegram_update_with_created_person, None)
        test_telegram_update_with_created_person.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.INFO_FILL_COMPLETED
        )

    @pytest.mark.django_db
    def test_skip_date_of_birth(self, test_telegram_update_with_created_person):
        skip_date_of_birth(test_telegram_update_with_created_person, None)
        test_telegram_update_with_created_person.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.INFO_FILL_COMPLETED
        )

    @pytest.mark.django_db
    def test_set_phone_cmd(self, test_telegram_update):
        next_state = set_phone_cmd(test_telegram_update, None)
        assert next_state == BotState.PHONE

    @pytest.mark.django_db
    def test_set_phone_msg_when_wrong_format(self, test_telegram_update):
        type(test_telegram_update.message).text = PropertyMock(return_value="abc")
        set_phone_msg(test_telegram_update, None)
        test_telegram_update.message.reply_text.assert_called_once_with(PersonInfoResponseFormats.PHONE_INVALID_FORMAT)

    @pytest.mark.django_db
    def test_set_phone_msg_when_correct(self, test_telegram_update):
        new_phone = "11111111111"
        type(test_telegram_update.message).text = PropertyMock(return_value=new_phone)
        set_phone_msg(test_telegram_update, None)
        person = Person.objects.get(telegram_chat_id=test_telegram_update.message.chat_id)
        assert person.phone == new_phone

    @pytest.mark.django_db
    def test_set_phone_msg_when_such_phone_exists(self, test_telegram_update):
        new_phone = "11111111111"
        Person.objects.create(telegram_chat_id=333, phone=new_phone)
        type(test_telegram_update.message).text = PropertyMock(return_value=new_phone)
        set_phone_msg(test_telegram_update, None)
        test_telegram_update.message.reply_text.assert_called_once_with(PersonInfoResponseFormats.PHONE_ALREADY_EXISTS)

    @pytest.mark.django_db
    def test_set_name_cmd_when_no_phone(self, test_telegram_update):
        next_state = set_name_cmd(test_telegram_update, None)
        assert next_state != BotState.NAME
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED
        )

    @pytest.mark.django_db
    def test_set_name_cmd_when_have_phone(self, test_telegram_update):
        Person.objects.create(telegram_chat_id=test_telegram_update.message.chat_id, phone="11111111111")
        type(test_telegram_update.message).text = PropertyMock(return_value="John")
        next_state = set_name_cmd(test_telegram_update, None)
        assert next_state == BotState.NAME

    @pytest.mark.django_db
    def test_set_name_msg(self, test_telegram_update_with_created_person):
        new_name = "John"
        type(test_telegram_update_with_created_person.message).text = PropertyMock(return_value=new_name)
        set_name_msg(test_telegram_update_with_created_person, None)
        person = Person.objects.get(telegram_chat_id=test_telegram_update_with_created_person.message.chat_id)
        assert person.name == new_name

    @pytest.mark.django_db
    def test_set_date_of_birth_cmd_when_no_phone(self, test_telegram_update):
        next_state = set_date_of_birth_cmd(test_telegram_update, None)
        assert next_state != BotState.DATE_OF_BIRTH
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED
        )

    @pytest.mark.django_db
    def test_set_date_of_birth_cmd_when_have_phone(self, test_telegram_update):
        Person.objects.create(telegram_chat_id=test_telegram_update.message.chat_id, phone="11111111111")
        type(test_telegram_update.message).text = PropertyMock(return_value="2022-01-02")
        next_state = set_date_of_birth_cmd(test_telegram_update, None)
        assert next_state == BotState.DATE_OF_BIRTH

    @pytest.mark.django_db
    def test_set_date_of_birth_msg(self, test_telegram_update_with_created_person):
        new_date = "2022-01-02"
        type(test_telegram_update_with_created_person.message).text = PropertyMock(return_value=new_date)
        set_date_of_birth_msg(test_telegram_update_with_created_person, None)
        person = Person.objects.get(telegram_chat_id=test_telegram_update_with_created_person.message.chat_id)
        assert person.date_of_birth.strftime("%Y-%m-%d") == new_date

    @pytest.mark.django_db
    def test_set_password_cmd_when_no_phone(self, test_telegram_update):
        next_state = set_password_cmd(test_telegram_update, None)
        assert next_state != BotState.PASSWORD
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED
        )

    @pytest.mark.django_db
    def test_set_password_cmd_when_have_phone(self, test_telegram_update):
        Person.objects.create(telegram_chat_id=test_telegram_update.message.chat_id, phone="11111111111")
        type(test_telegram_update.message).text = PropertyMock(return_value="2022-01-02")
        next_state = set_password_cmd(test_telegram_update, None)
        assert next_state == BotState.PASSWORD

    @pytest.mark.django_db
    def test_set_password_msg(self, test_telegram_update_with_created_person):
        new_password = "1111"
        type(test_telegram_update_with_created_person.message).text = PropertyMock(return_value=new_password)
        set_password_msg(test_telegram_update_with_created_person, None)
        person = Person.objects.get(telegram_chat_id=test_telegram_update_with_created_person.message.chat_id)
        assert check_password(new_password, person.password)

    @pytest.mark.django_db
    def test_me_when_no_phone(self, test_telegram_update):
        me(test_telegram_update, None)
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED
        )

    @pytest.mark.django_db
    def test_me(self, test_telegram_update):
        Person.objects.create(telegram_chat_id=test_telegram_update.message.chat_id, name="John", phone="11111111111")
        me(test_telegram_update, None)
        call_args = test_telegram_update.message.reply_text.call_args.args
        assert len(call_args) == 1
        assert "John" in call_args[0]
        assert "11111111111" in call_args[0]

    @pytest.mark.django_db
    def test_help(self, test_telegram_update):
        help(test_telegram_update, None)
        call_args = test_telegram_update.message.reply_text.call_args.args
        assert len(call_args) == 1
        assert "/me" in call_args[0]
