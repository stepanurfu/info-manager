from unittest.mock import MagicMock, PropertyMock

import pytest

from app.internal.bank_account.db.repositories import BankAccountRepository
from app.internal.bank_card.db.repositories import BankCardRepository
from app.internal.person.db.models import Person


@pytest.fixture(scope="function")
def test_telegram_update():
    message_mock = MagicMock()
    message_mock.reply_text = MagicMock(return_value=None)
    message_mock.reply_document = MagicMock(return_value=None)
    type(message_mock).chat_id = PropertyMock(return_value=123)
    update_mock = MagicMock()
    type(update_mock).message = PropertyMock(return_value=message_mock)
    return update_mock


@pytest.fixture(scope="function")
def test_telegram_update_with_created_person():
    person = Person.objects.create(pk=123, telegram_chat_id=123, phone="11111111111")
    message_mock = MagicMock()
    message_mock.reply_text = MagicMock(return_value=None)
    type(message_mock).chat_id = PropertyMock(return_value=person.telegram_chat_id)
    update_mock = MagicMock()
    type(update_mock).message = PropertyMock(return_value=message_mock)
    return update_mock


@pytest.fixture(scope="function")
def test_telegram_update_empty():
    update_mock = MagicMock()
    message_prop = PropertyMock(return_value=None)
    type(update_mock).message = message_prop
    return update_mock


@pytest.fixture(scope="function")
def test_bank_account_repo():
    return BankAccountRepository()


@pytest.fixture(scope="function")
def test_bank_card_repo():
    return BankCardRepository()


@pytest.fixture(scope="function")
def test_context_mock_and_document():
    file_mock = MagicMock()
    file_mock.download_as_bytearray = MagicMock(return_value=b"")

    bot_mock = MagicMock()
    bot_mock.get_file = MagicMock(return_value=file_mock)

    context_mock = MagicMock()
    type(context_mock).bot = PropertyMock(return_value=bot_mock)

    document_mock = MagicMock()
    type(document_mock).file_name = PropertyMock(return_value="file.jpg")
    type(document_mock).file_id = PropertyMock(return_value=0)

    return context_mock, document_mock
