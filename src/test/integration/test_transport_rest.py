import json

import pytest
from django.urls import reverse_lazy

from app.internal.authentication.db.repositories import IssuedTokenRepository
from app.internal.authentication.domain.services import AuthService


class TestRest:
    @pytest.mark.django_db
    def test_get_my_info(self, client, test_person):
        url = reverse_lazy("api-1.0.0:my_info")
        _auth_service = AuthService(IssuedTokenRepository())
        jwt_tokens = _auth_service.generate_jwt_tokens(test_person)
        auth_headers = {
            "HTTP_AUTHORIZATION": "Bearer " + jwt_tokens.access_token,
        }
        response = client.get(url, **auth_headers)
        assert response.status_code == 200
        assert str(test_person.telegram_chat_id) in response.content.decode("utf-8")

    @pytest.mark.django_db
    def test_get_my_info_when_not_authorized(self, client):
        url = reverse_lazy("api-1.0.0:my_info")
        response = client.get(url)
        assert response.status_code == 401
