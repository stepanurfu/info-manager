from unittest.mock import PropertyMock

import pytest

from app.internal.bank_card.db.models import BankCard
from app.internal.bank_card.domain.entities import BankCardIn
from app.internal.bank_transaction.db.models import BankTransaction
from app.internal.bot.base_handlers import BotState
from app.internal.bot.handlers.bank_account_handlers import (
    show_account_balance_cmd,
    show_account_balance_msg,
    show_balance_cmd,
    show_card_balance_cmd,
    show_card_balance_msg,
    show_new_transactions,
    show_person_cards_cmd,
    show_person_cards_msg,
    show_statements_by_account_cmd,
    show_statements_by_account_msg,
    show_statements_by_card_cmd,
    show_statements_by_card_msg,
    show_transfer_relative_names,
    transfer_by_card_cmd,
    transfer_by_card_msg,
    transfer_by_card_with_postcard_cmd,
    transfer_by_card_with_postcard_msg,
)
from app.internal.bot.response_messages import PersonInfoResponseFormats
from app.internal.post_card.db.repositories import PostCardRepository
from app.internal.post_card.domain.entities import PostCardIn


class TestBotBankAccountHandlers:
    @pytest.mark.django_db
    def test_show_balance_cmd_when_no_phone(self, test_telegram_update):
        next_state = show_balance_cmd(test_telegram_update, None)
        assert next_state != BotState.BANK_ACCOUNT
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED
        )

    @pytest.mark.django_db
    def test_show_balance_cmd_when_have_phone(self, test_telegram_update, test_person_with_account_and_card):
        next_state = show_balance_cmd(test_telegram_update, None)
        assert next_state == BotState.BANK_ACCOUNT
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.ENTER_BANK_ACCOUNT_CARD_NUMBER
        )

    @pytest.mark.django_db
    def test_show_card_balance_cmd_when_no_phone(self, test_telegram_update):
        next_state = show_card_balance_cmd(test_telegram_update, None)
        assert next_state != BotState.BANK_CARD
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED
        )

    @pytest.mark.django_db
    def test_show_card_balance_cmd_when_have_phone(self, test_telegram_update, test_person_with_account_and_card):
        next_state = show_card_balance_cmd(test_telegram_update, None)
        assert next_state == BotState.BANK_CARD
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.ENTER_BANK_CARD_NUMBER
        )

    @pytest.mark.django_db
    def test_show_card_balance_msg(self, test_telegram_update, test_person_with_account_and_card):
        person, account, card = test_person_with_account_and_card
        card.balance = 100
        card.save()
        type(test_telegram_update.message).text = PropertyMock(return_value=card.number)
        show_card_balance_msg(test_telegram_update, None)
        call_args = test_telegram_update.message.reply_text.call_args.args
        assert len(call_args) == 1
        assert PersonInfoResponseFormats.BANK_CARD_BALANCE.format(card.balance) in call_args[0]

    @pytest.mark.django_db
    def test_show_card_balance_msg_when_number_not_exist(self, test_telegram_update, test_person_with_account_and_card):
        type(test_telegram_update.message).text = PropertyMock(return_value="123")
        show_card_balance_msg(test_telegram_update, None)
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.BANK_ACCOUNT_CARD_NUMBER_NOT_FOUND
        )

    @pytest.mark.django_db
    def test_show_account_balance_cmd_when_no_phone(self, test_telegram_update):
        next_state = show_account_balance_cmd(test_telegram_update, None)
        assert next_state != BotState.BANK_ACCOUNT
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED
        )

    @pytest.mark.django_db
    def test_show_account_balance_cmd_when_have_phone(self, test_telegram_update, test_person_with_account_and_card):
        next_state = show_account_balance_cmd(test_telegram_update, None)
        assert next_state == BotState.BANK_ACCOUNT
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.ENTER_BANK_ACCOUNT_NUMBER
        )

    @pytest.mark.django_db
    def test_show_account_balance_msg(self, test_telegram_update, test_person_with_account_and_card):
        person, account, card = test_person_with_account_and_card
        card.balance = 100
        card.save()
        type(test_telegram_update.message).text = PropertyMock(return_value=account.number)
        show_account_balance_msg(test_telegram_update, None)
        call_args = test_telegram_update.message.reply_text.call_args.args
        assert len(call_args) == 1
        assert PersonInfoResponseFormats.BANK_ACCOUNT_CARD_NUMBER_BALANCE.format(card.balance) in call_args[0]

    @pytest.mark.django_db
    def test_show_account_balance_msg_when_number_not_exist(
        self, test_telegram_update, test_person_with_account_and_card
    ):
        type(test_telegram_update.message).text = PropertyMock(return_value="123")
        show_account_balance_msg(test_telegram_update, None)
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.BANK_ACCOUNT_CARD_NUMBER_NOT_FOUND
        )

    @pytest.mark.django_db
    def test_transfer_by_card_cmd_when_no_phone(self, test_telegram_update):
        next_state = transfer_by_card_cmd(test_telegram_update, None)
        assert next_state != BotState.TRANSFER
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED
        )

    @pytest.mark.django_db
    def test_transfer_by_card_cmd_when_have_phone(self, test_telegram_update, test_person_with_account_and_card):
        next_state = transfer_by_card_cmd(test_telegram_update, None)
        assert next_state == BotState.TRANSFER
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.ENTER_TRANSFER_INFO_BY_BANK_CARD
        )

    @pytest.mark.django_db
    def test_transfer_by_card_msg(self, test_telegram_update, test_person_with_account_and_card, test_bank_card_repo):
        person, account, card = test_person_with_account_and_card
        init_balance = 100
        amount = 20
        card.balance = init_balance
        card.save()
        other_card = test_bank_card_repo.add_bank_card(BankCardIn(number="3" * 16, code=123, bank_account=account.pk))
        args = f"{card.number} {other_card.number} {amount}"
        type(test_telegram_update.message).text = PropertyMock(return_value=args)
        transfer_by_card_msg(test_telegram_update, None)
        card = BankCard.objects.get(pk=card.pk)
        other_card = BankCard.objects.get(pk=other_card.pk)
        assert card.balance == init_balance - amount
        assert other_card.balance == amount

    @pytest.mark.django_db
    def test_show_person_cards_cmd_when_no_phone(self, test_telegram_update):
        next_state = show_person_cards_cmd(test_telegram_update, None)
        assert next_state != BotState.BANK_CARD
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED
        )

    @pytest.mark.django_db
    def test_show_person_cards_cmd_when_have_phone(self, test_telegram_update, test_person_with_account_and_card):
        next_state = show_person_cards_cmd(test_telegram_update, None)
        assert next_state == BotState.BANK_CARD
        test_telegram_update.message.reply_text.assert_called_once_with(PersonInfoResponseFormats.ENTER_USER_ID)

    @pytest.mark.django_db
    def test_show_person_cards_msg(self, test_telegram_update, test_person_with_account_and_card):
        person, account, card = test_person_with_account_and_card
        type(test_telegram_update.message).text = PropertyMock(return_value=str(person.telegram_chat_id))
        show_person_cards_msg(test_telegram_update, None)
        call_args = test_telegram_update.message.reply_text.call_args.args
        assert len(call_args) == 1
        assert account.number in call_args[0]
        assert card.number in call_args[0]

    @pytest.mark.django_db
    def test_show_statements_by_card_cmd_when_no_phone(self, test_telegram_update):
        next_state = show_statements_by_card_cmd(test_telegram_update, None)
        assert next_state != BotState.STATEMENTS
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED
        )

    @pytest.mark.django_db
    def test_show_statements_by_card_cmd_when_have_phone(self, test_telegram_update, test_person_with_account_and_card):
        next_state = show_statements_by_card_cmd(test_telegram_update, None)
        assert next_state == BotState.STATEMENTS
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.ENTER_BANK_CARD_NUMBER_WITH_DATE_RANGE
        )

    @pytest.mark.django_db
    def test_show_statements_by_card_msg(self, test_telegram_update, test_person_with_account_and_card):
        person, account, card = test_person_with_account_and_card
        type(test_telegram_update.message).text = PropertyMock(return_value=card.number)
        show_statements_by_card_msg(test_telegram_update, None)
        call_args = test_telegram_update.message.reply_text.call_args.args
        assert len(call_args) == 1
        assert str(card.balance) in call_args[0]
        assert card.number in call_args[0]

    @pytest.mark.django_db
    def test_show_statements_by_account_cmd_when_no_phone(self, test_telegram_update):
        next_state = show_statements_by_account_cmd(test_telegram_update, None)
        assert next_state != BotState.STATEMENTS
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED
        )

    @pytest.mark.django_db
    def test_show_statements_by_account_cmd_when_have_phone(
        self, test_telegram_update, test_person_with_account_and_card
    ):
        next_state = show_statements_by_account_cmd(test_telegram_update, None)
        assert next_state == BotState.STATEMENTS
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.ENTER_BANK_ACCOUNT_NUMBER_WITH_DATE_RANGE
        )

    @pytest.mark.django_db
    def test_show_statements_by_account_msg(self, test_telegram_update, test_person_with_account_and_card):
        person, account, card = test_person_with_account_and_card
        type(test_telegram_update.message).text = PropertyMock(return_value=account.number)
        show_statements_by_account_msg(test_telegram_update, None)
        call_args = test_telegram_update.message.reply_text.call_args.args
        assert len(call_args) == 1
        assert str(card.balance) in call_args[0]
        assert account.number in call_args[0]
        assert card.number in call_args[0]

    @pytest.mark.django_db
    def test_show_transfer_relative_names(
        self, test_telegram_update, test_person_with_account_and_card, test_person_with_account_and_card_2
    ):
        person, account, card = test_person_with_account_and_card
        person2, account2, card2 = test_person_with_account_and_card_2
        BankTransaction.objects.create(bank_card_from=card, bank_card_to=card2, amount=1)

        show_transfer_relative_names(test_telegram_update, None)
        call_args = test_telegram_update.message.reply_text.call_args.args
        assert len(call_args) == 1
        assert person2.name in call_args[0]
        assert person.name not in call_args[0]

    @pytest.mark.django_db
    def test_show_new_transactions_without_postcard(
        self, test_telegram_update, test_person_with_account_and_card, test_person_with_account_and_card_2
    ):
        person, account, card = test_person_with_account_and_card
        person2, account2, card2 = test_person_with_account_and_card_2
        BankTransaction.objects.create(bank_card_from=card, bank_card_to=card2, amount=1)
        show_new_transactions(test_telegram_update, None)
        call_args = test_telegram_update.message.reply_text.call_args.args
        assert len(call_args) == 1
        assert card.number in call_args[0]
        assert card2.number in call_args[0]

    @pytest.mark.django_db
    def test_show_new_transactions_with_postcard(
        self, test_telegram_update, test_person_with_account_and_card, test_person_with_account_and_card_2
    ):
        person, account, card = test_person_with_account_and_card
        person2, account2, card2 = test_person_with_account_and_card_2

        postcard = PostCardRepository().add_post_card(PostCardIn(content=b""), "1")
        BankTransaction.objects.create(bank_card_from=card, bank_card_to=card2, amount=1, postcard=postcard)

        show_new_transactions(test_telegram_update, None)
        call_args = test_telegram_update.message.reply_text.call_args.args
        assert len(call_args) == 1
        call_args = test_telegram_update.message.reply_document.call_args.args
        assert len(call_args) == 1

    @pytest.mark.django_db
    def test_transfer_by_card_with_postcard_cmd_when_no_phone(self, test_telegram_update):
        next_state = transfer_by_card_with_postcard_cmd(test_telegram_update, None)
        assert next_state != BotState.TRANSFER
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED
        )

    @pytest.mark.django_db
    def test_transfer_by_card_with_postcard_cmd_when_have_phone(
        self, test_telegram_update, test_person_with_account_and_card
    ):
        next_state = transfer_by_card_with_postcard_cmd(test_telegram_update, None)
        assert next_state == BotState.TRANSFER
        test_telegram_update.message.reply_text.assert_called_once_with(
            PersonInfoResponseFormats.ENTER_TRANSFER_INFO_BY_BANK_CARD_WITH_POSTCARD
        )

    @pytest.mark.django_db
    def test_transfer_by_card_with_postcard_msg(
        self,
        test_telegram_update,
        test_person_with_account_and_card,
        test_bank_card_repo,
        test_context_mock_and_document,
    ):
        person, account, card = test_person_with_account_and_card
        init_balance = 100
        amount = 20
        card.balance = init_balance
        card.save()
        other_card = test_bank_card_repo.add_bank_card(BankCardIn(number="3" * 16, code=123, bank_account=account.pk))
        args = f"{card.number} {other_card.number} {amount}"
        type(test_telegram_update.message).caption = PropertyMock(return_value=args)
        context_mock, document_mock = test_context_mock_and_document
        type(test_telegram_update.message).document = PropertyMock(return_value=document_mock)
        transfer_by_card_with_postcard_msg(test_telegram_update, context_mock)
        card = BankCard.objects.get(pk=card.pk)
        other_card = BankCard.objects.get(pk=other_card.pk)
        assert card.balance == init_balance - amount
        assert other_card.balance == amount
