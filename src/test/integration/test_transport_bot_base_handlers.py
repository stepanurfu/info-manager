from unittest.mock import MagicMock

import pytest
from telegram.ext import ConversationHandler

from app.internal.bot.base_handlers import cancel, create_conversation_handler, if_have_message
from app.internal.bot.response_messages import PersonInfoResponseFormats


class TestBotBaseHandlers:
    def test_cancel(self, test_telegram_update):
        cancel(test_telegram_update, None)
        test_telegram_update.message.reply_text.assert_called_once_with(PersonInfoResponseFormats.CANCEL)

    def test_if_have_message_when_message_exist(self, test_telegram_update: MagicMock):
        func_mock = MagicMock(return_value=None)
        wrapped = if_have_message(func_mock)
        wrapped(test_telegram_update, None)
        func_mock.assert_called_once()

    def test_if_have_message_when_no_message(self, test_telegram_update_empty: MagicMock):
        func_mock = MagicMock(return_value=None)
        wrapped = if_have_message(func_mock)
        wrapped(test_telegram_update_empty, None)
        func_mock.assert_not_called()

    def test_create_conversation_handler(self):
        state_handlers = {PersonInfoResponseFormats.CANCEL: lambda x: None}
        actual = create_conversation_handler("cmd", lambda x: None, state_handlers)
        assert isinstance(actual, ConversationHandler)
