import datetime
import time

import pytest

from app.internal.bank_account.db.repositories import BankAccountRepository
from app.internal.bank_card.db.models import BankCard
from app.internal.bank_card.db.repositories import BankCardRepository
from app.internal.bank_card.domain.entities import BankCardIn
from app.internal.bank_transaction.db.models import BankTransaction
from app.internal.bank_transaction.db.repositories import BankTransactionRepository
from app.internal.bank_transaction.domain.services import BankTransactionService
from app.internal.common.utils import add_timezone
from app.internal.person.db.repositories import PersonRepository


class TestBankService:
    @pytest.mark.django_db
    @pytest.mark.parametrize("test_person_with_account_and_card", [("Juan", "1" * 20, "2" * 16)], indirect=True)
    def test_get_person_cards_when_has_account_and_card(
        self, test_person_with_account_and_card, test_bank_card_repo: BankCardRepository
    ):
        person, account, card = test_person_with_account_and_card
        actual = test_bank_card_repo.get_person_cards(person)
        assert account.number in actual.keys()
        assert card.number in actual[account.number]

    @pytest.mark.django_db
    def test_format_person_cards_when_no_account_and_card(self, test_person, test_bank_card_repo: BankCardRepository):
        actual = test_bank_card_repo.get_person_cards(test_person)
        assert len(actual) == 0

    @pytest.mark.django_db
    def test_create_card_for_account(self, test_person_with_account_and_card, test_bank_card_repo: BankCardRepository):
        person, account, card = test_person_with_account_and_card
        new_card_number = "3" * 16
        new_card = test_bank_card_repo.add_bank_card(
            BankCardIn(number=new_card_number, code=123, bank_account=account.pk)
        )
        assert new_card.number == new_card_number

    @pytest.mark.django_db
    def test_get_bank_account_by_card_number(
        self, test_person_with_account_and_card, test_bank_account_repo: BankAccountRepository
    ):
        person, account, card = test_person_with_account_and_card
        actual = test_bank_account_repo.get_bank_account_by_card_number(person, card.number)
        assert account.number == actual.number

    @pytest.mark.django_db
    def test_get_bank_account_by_number(
        self, test_person_with_account_and_card, test_bank_account_repo: BankAccountRepository
    ):
        person, account, card = test_person_with_account_and_card
        actual = test_bank_account_repo.get_bank_account_by_number(person, account.number)
        assert account.number == actual.number

    @pytest.mark.django_db
    def test_get_bank_card_by_number(self, test_person_with_account_and_card, test_bank_card_repo: BankCardRepository):
        person, account, card = test_person_with_account_and_card
        actual = test_bank_card_repo.get_bank_card_by_number(person, card.number)
        assert card.number == actual.number

    @pytest.mark.django_db
    def test_get_bank_card_by_number_when_wrong_number(
        self, test_person_with_account_and_card, test_bank_card_repo: BankCardRepository
    ):
        person, account, card = test_person_with_account_and_card
        actual = test_bank_card_repo.get_bank_card_by_number(person, "6" * 16)
        assert actual is None

    @pytest.mark.django_db
    def test_get_bank_account_balance(
        self, test_person_with_account_and_card, test_bank_account_repo: BankAccountRepository
    ):
        person, account, card = test_person_with_account_and_card
        card.balance = 100
        card.save()
        actual = test_bank_account_repo.get_bank_account_balance(account)
        assert actual == 100

    @pytest.mark.django_db
    def test_transfer_by_card(
        self,
        test_person_with_account_and_card,
        test_bank_card_repo: BankCardRepository,
        test_bank_transaction_repo: BankTransactionRepository,
    ):
        person, account, card = test_person_with_account_and_card
        card.balance = 100
        card.save()
        new_card_number = "3" * 16
        new_card = test_bank_card_repo.add_bank_card(
            BankCardIn(number=new_card_number, code=123, bank_account=account.pk)
        )
        success = test_bank_transaction_repo.transfer_by_card(person, card.number, new_card.number, 50)
        assert success
        card = test_bank_card_repo.get_bank_card_by_number(person, card.number)
        new_card = test_bank_card_repo.get_bank_card_by_number(person, new_card.number)
        assert card.balance == 50
        assert new_card.balance == 50

    @pytest.mark.django_db
    def test_get_transaction_info_by_card(
        self, test_person_with_account_and_card, test_bank_transaction_repo: BankTransactionRepository
    ):
        person, account, card = test_person_with_account_and_card
        init_balance = 100
        amount = 1
        card.balance = init_balance
        card.save()
        other_card = BankCard.objects.create(pk=101, number="1234" * 4, bank_account=account, code=123)
        BankTransaction.objects.create(bank_card_from=card, bank_card_to=other_card, amount=amount)
        bank_transaction_accounting = test_bank_transaction_repo.get_transaction_info_by_card(card)
        assert bank_transaction_accounting.card_number == card.number
        assert len(bank_transaction_accounting.bank_transaction_infos) == 1
        assert bank_transaction_accounting.bank_transaction_infos[0].card_from == card.number
        assert bank_transaction_accounting.bank_transaction_infos[0].card_to == other_card.number
        assert bank_transaction_accounting.bank_transaction_infos[0].amount == amount

    @pytest.mark.django_db
    def test_get_transaction_info_by_card_many(
        self, test_person_with_account_and_card, test_bank_transaction_repo: BankTransactionRepository
    ):
        person, account, card = test_person_with_account_and_card
        init_balance = 100
        amount = 1
        card.balance = init_balance
        card.save()
        other_card = BankCard.objects.create(pk=101, number="1234" * 4, bank_account=account, code=123)
        BankTransaction.objects.create(bank_card_from=card, bank_card_to=other_card, amount=amount)
        BankTransaction.objects.create(bank_card_from=other_card, bank_card_to=card, amount=amount)
        bank_transaction_accounting = test_bank_transaction_repo.get_transaction_info_by_card(card)
        assert bank_transaction_accounting.card_number == card.number
        assert len(bank_transaction_accounting.bank_transaction_infos) == 2

    @pytest.mark.django_db
    def test_get_transaction_info_by_card_number(
        self, test_person_with_account_and_card, test_bank_transaction_service: BankTransactionService
    ):
        person, account, card = test_person_with_account_and_card
        init_balance = 100
        amount = 1
        card.balance = init_balance
        card.save()
        other_card = BankCard.objects.create(pk=101, number="1234" * 4, bank_account=account, code=123)
        BankTransaction.objects.create(bank_card_from=card, bank_card_to=other_card, amount=amount)
        bank_transaction_accounting = test_bank_transaction_service.get_transaction_info_by_card_number(
            person, card.number
        )
        assert bank_transaction_accounting.card_number == card.number
        assert len(bank_transaction_accounting.bank_transaction_infos) == 1
        assert bank_transaction_accounting.bank_transaction_infos[0].card_from == card.number
        assert bank_transaction_accounting.bank_transaction_infos[0].card_to == other_card.number
        assert bank_transaction_accounting.bank_transaction_infos[0].amount == amount

    @pytest.mark.django_db
    def test_get_transaction_info_by_card_number_when_number_not_exist(
        self, test_person_with_account_and_card, test_bank_transaction_service: BankTransactionService
    ):
        person, account, card = test_person_with_account_and_card
        bank_transaction_accounting = test_bank_transaction_service.get_transaction_info_by_card_number(person, "123")
        assert bank_transaction_accounting is None

    @pytest.mark.django_db
    def test_get_transaction_info_by_account_number(
        self, test_person_with_account_and_card, test_bank_transaction_service: BankTransactionService
    ):
        person, account, card = test_person_with_account_and_card
        init_balance = 100
        amount = 1
        card.balance = init_balance
        card.save()
        other_card = BankCard.objects.create(pk=101, number="1234" * 4, bank_account=account, code=123)
        BankTransaction.objects.create(bank_card_from=card, bank_card_to=other_card, amount=amount)
        bank_transaction_accountings_per_card = test_bank_transaction_service.get_transaction_info_by_account_number(
            person, account.number
        )
        assert len(bank_transaction_accountings_per_card) == 2
        assert len(bank_transaction_accountings_per_card[0].bank_transaction_infos) == 1
        assert len(bank_transaction_accountings_per_card[1].bank_transaction_infos) == 1

    @pytest.mark.django_db
    def test_get_transaction_info_by_account_number_when_number_not_exist(
        self, test_person_with_account_and_card, test_bank_transaction_service: BankTransactionService
    ):
        person, account, card = test_person_with_account_and_card
        bank_transaction_accounting = test_bank_transaction_service.get_transaction_info_by_account_number(
            person, "123"
        )
        assert bank_transaction_accounting is None

    @pytest.mark.django_db
    def test_get_all_transfer_relative_names_to_self(
        self, test_person_with_account_and_card, test_bank_transaction_repo: BankTransactionRepository
    ):
        person, account, card = test_person_with_account_and_card
        other_card = BankCard.objects.create(pk=101, number="1234" * 4, bank_account=account, code=123)
        BankTransaction.objects.create(bank_card_from=card, bank_card_to=other_card, amount=1)
        actual = test_bank_transaction_repo.get_all_transfer_relative_names(person)
        assert person.name in actual

    @pytest.mark.django_db
    def test_get_all_transfer_relative_names(
        self,
        test_person_with_account_and_card,
        test_person_with_account_and_card_2,
        test_bank_transaction_repo: BankTransactionRepository,
    ):
        person, account, card = test_person_with_account_and_card
        person2, account2, card2 = test_person_with_account_and_card_2
        BankTransaction.objects.create(bank_card_from=card, bank_card_to=card2, amount=1)
        actual = test_bank_transaction_repo.get_all_transfer_relative_names(person)
        assert person.name not in actual
        assert person2.name in actual

        actual = test_bank_transaction_repo.get_all_transfer_relative_names(person2)
        assert person.name in actual
        assert person2.name not in actual

    @pytest.mark.django_db
    def test_get_new_transactions(
        self,
        test_person_with_account_and_card,
        test_person_with_account_and_card_2,
        test_bank_transaction_repo: BankTransactionRepository,
        test_person_repo: PersonRepository,
    ):
        person, account, card = test_person_with_account_and_card
        person2, account2, card2 = test_person_with_account_and_card_2
        card.balance = 1000
        card.save()
        test_bank_transaction_repo.transfer_by_card(person, card.number, card2.number, 1)

        actual = test_bank_transaction_repo.get_new_transactions(person)
        assert len(actual) == 1
        assert len(actual[0].bank_transaction_infos) == 1
        actual = test_bank_transaction_repo.get_new_transactions(person2)
        assert len(actual) == 1
        assert len(actual[0].bank_transaction_infos) == 1
        time.sleep(2)
        utc_new_time = datetime.datetime.now(datetime.timezone.utc)
        test_person_repo.update_last_transaction_check(person, utc_new_time)
        person = test_person_repo.get_person_by_id(person.pk)

        actual = test_bank_transaction_repo.get_new_transactions(person)
        assert len(actual) == 1
        assert len(actual[0].bank_transaction_infos) == 0
