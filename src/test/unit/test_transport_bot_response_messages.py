import pytest

from app.internal.bot.response_messages import build_help_message, build_person_info_message
from app.internal.person.db.models import Person


class TestResponseMessages:
    def test_build_person_info_message(self):
        person = Person(name="Juan", telegram_chat_id=100, phone="11111111111")
        actual = build_person_info_message(person)
        assert person.name in actual
        assert str(person.telegram_chat_id) in actual
        assert person.phone in actual
        assert "birth" in actual
        assert "Phone" in actual

    def test_build_help_message(self):
        expected_commands = [
            "/cancel",
            "/set_phone",
            "/set_name",
            "/set_date_of_birth",
            "/me",
            "/show_account_balance",
            "/show_card_balance",
            "/transfer_by_card",
            "/transfer_by_card_with_postcard",
            "/show_favorite_users_list",
            "/add_to_favorite_users_list",
            "/remove_from_favorite_users_list",
            "/show_new_transactions",
        ]
        link = "https://example.com/api/me?id=123"
        actual = build_help_message(link)
        assert link in actual
        for command in expected_commands:
            assert command in actual
