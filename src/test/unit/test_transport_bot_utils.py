import datetime

import pytest

from app.internal.bot.utils import (
    StatementInfo,
    TransferInfo,
    build_fill_prompt_message,
    build_fill_success_message,
    check_email,
    check_if_bank_account_number,
    normailze_date,
    normalize_phone,
)

test_trander_info = TransferInfo("1" * 16, "2" * 16, 500)
test_statement_info = StatementInfo("1" * 16, datetime.date(1970, 1, 1), datetime.date(1970, 1, 2))


class TestTransportBotUtils:
    def test_build_fill_prompt_message_mandatory_no_prev_value(self):
        field = "name"
        actual = build_fill_prompt_message(field, is_mandatory=True)
        assert "mandatory" in actual
        assert field in actual

    def test_build_fill_prompt_message_mandatory_with_prev_value(self):
        field = "name"
        value = "Juan"
        actual = build_fill_prompt_message(field, value, is_mandatory=True)
        assert field in actual
        assert value in actual

    def test_build_fill_success_message_when_no_prev_value(self):
        field = "name"
        prev_value = None
        new_value = "Shiny"
        actual = build_fill_success_message(field, prev_value, new_value)
        assert field in actual
        assert new_value in actual
        assert "saved" in actual

    def test_build_fill_success_message_when_changed(self):
        field = "name"
        prev_value = "Juan"
        new_value = "Shiny"
        actual = build_fill_success_message(field, prev_value, new_value)
        assert field in actual
        assert new_value in actual
        assert "changed" in actual

    @pytest.mark.parametrize("date", ["2022-01-02", "02.01.2022", "02/01/2022"])
    def test_normailze_date(self, date):
        assert normailze_date(date) == "2022-01-02"

    @pytest.mark.parametrize(
        "phone", ["66677788899", "6(667)7788899", "6-667-778-88-99", "6667.778.88.99", "6-(667)-77888-99"]
    )
    def test_normalize_phone(self, phone):
        assert normalize_phone(phone) == "66677788899"

    @pytest.mark.parametrize(
        "email,is_valid",
        [
            ("a@a.com", True),
            ("juan@example.com", True),
            ("juan.org@example.me", True),
            ("1231231", False),
            ("example.com", False),
            ("@", False),
            ("13@13", False),
        ],
    )
    def test_check_email(self, email, is_valid):
        assert check_email(email) == is_valid

    @pytest.mark.parametrize(
        "number,is_valid", [("1" * 20, True), ("1234" * 5, True), ("1" * 19, False), ("1" * 21, False), ("", False)]
    )
    def test_check_if_bank_account_number(self, number, is_valid):
        assert check_if_bank_account_number(number) == is_valid

    @pytest.mark.parametrize(
        "args_str,transfer_info",
        [
            (
                f"{test_trander_info.card_from} {test_trander_info.card_to} {test_trander_info.amount}",
                test_trander_info,
            ),
            (
                f"  \n\n{test_trander_info.card_from}   {test_trander_info.card_to} \n  {test_trander_info.amount}  \t",
                test_trander_info,
            ),
            ("", None),
            (f"{test_trander_info.card_from}", None),
            (f"{test_trander_info.card_from} {test_trander_info.card_to}", None),
            (f"{test_trander_info.card_from} {test_trander_info.card_to} aaa", None),
        ],
    )
    def test_parse_transfer_arguments(self, args_str: str, transfer_info: TransferInfo):
        actual_transfer_info = TransferInfo.parse_transfer_arguments(args_str)
        if transfer_info is None:
            assert actual_transfer_info is None
        else:
            actual_transfer_info.card_from = transfer_info.card_from
            actual_transfer_info.card_to = transfer_info.card_to
            actual_transfer_info.amount = transfer_info.amount

    @pytest.mark.parametrize(
        "args_str,statement_info",
        [
            (
                f"{test_statement_info.number} {test_statement_info.start_date.strftime('%Y-%m-%d')} {test_statement_info.end_date.strftime('%Y-%m-%d')}",
                test_statement_info,
            ),
            (
                f"  \n\n{test_statement_info.number}   {test_statement_info.start_date.strftime('%Y-%m-%d')} \n  {test_statement_info.end_date.strftime('%Y-%m-%d')}  \t",
                test_statement_info,
            ),
            ("", None),
        ],
    )
    def test_parse_statement_arguments(self, args_str: str, statement_info: StatementInfo):
        actual_statement_info = StatementInfo.parse_statement_arguments(args_str)
        if statement_info is None:
            assert actual_statement_info is None
        else:
            assert actual_statement_info.number == statement_info.number
            assert actual_statement_info.start_date == statement_info.start_date
            assert actual_statement_info.end_date == statement_info.end_date
