import pytest

from app.internal.person.db.models import Person


@pytest.fixture(scope="function")
def test_person_with_favorite_list(id=1, name="Name1", other_id=2, other_name="Name2"):
    person = Person.objects.create(pk=id, telegram_chat_id=id, name=name, phone="22222222222")
    friend = Person.objects.create(pk=other_id, telegram_chat_id=other_id, name=other_name, phone="22222222223")
    person.favorite_users_list.add(friend)
    return person
