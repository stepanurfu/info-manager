import pytest
from django.contrib.auth.hashers import check_password

from app.internal.common.utils import PersonUpdate
from app.internal.person.db.repositories import Person, PersonRepository
from app.internal.person.presentation.telegram_format import format_favorite_users_list


class TestPersonService:
    @pytest.mark.django_db
    def test_add_or_update_only_present_fields_when_not_exist(self, test_person_repo: PersonRepository):
        update_dict = {"telegram_chat_id": 100, "name": "Juan"}
        person = test_person_repo.add_or_update_only_present_fields(
            update_dict["telegram_chat_id"], PersonUpdate(name=update_dict["name"], phone="11111111111")
        )
        assert person.telegram_chat_id == update_dict["telegram_chat_id"]
        assert person.name == update_dict["name"]

    @pytest.mark.django_db
    def test_add_or_update_only_present_fields_when_exist(
        self, test_person: Person, test_person_repo: PersonRepository
    ):
        person = test_person_repo.add_or_update_only_present_fields(
            test_person.telegram_chat_id, PersonUpdate(name="NewName")
        )
        assert person.telegram_chat_id == test_person.telegram_chat_id
        assert person.name == "NewName"

    @pytest.mark.django_db
    def test_get_person_by_id_when_exist(self, test_person: Person, test_person_repo: PersonRepository):
        person = test_person_repo.get_person_by_id(test_person.telegram_chat_id)
        assert person.telegram_chat_id == test_person.telegram_chat_id
        assert person.name == test_person.name

    @pytest.mark.django_db
    def test_get_favorite_users_list(self, test_person_with_favorite_list: Person, test_person_repo: PersonRepository):
        favorite_users_list = test_person_repo.get_favorite_users_list(test_person_with_favorite_list.telegram_chat_id)
        assert len(favorite_users_list) == 1

    @pytest.mark.django_db
    def test_add_to_favorite_users_list(
        self, test_person_with_favorite_list: Person, test_person_repo: PersonRepository
    ):
        success = test_person_repo.add_to_favorite_users_list(
            test_person_with_favorite_list.telegram_chat_id, test_person_with_favorite_list.telegram_chat_id
        )
        assert success
        favorite_users_list = test_person_repo.get_favorite_users_list(test_person_with_favorite_list.telegram_chat_id)
        assert len(favorite_users_list) == 2

    @pytest.mark.django_db
    def test_add_to_favorite_users_list_when_wrong_id(
        self, test_person_with_favorite_list: Person, test_person_repo: PersonRepository
    ):
        success = test_person_repo.add_to_favorite_users_list(123123132, test_person_with_favorite_list.pk)
        assert not success
        favorite_users_list = test_person_repo.get_favorite_users_list(test_person_with_favorite_list.pk)
        assert len(favorite_users_list) == 1

    @pytest.mark.django_db
    @pytest.mark.parametrize("test_person_with_favorite_list", [(1, "a", 2, "b")], indirect=True)
    def test_remove_from_favorite_users_list(
        self, test_person_with_favorite_list: Person, test_person_repo: PersonRepository
    ):
        success = test_person_repo.remove_from_favorite_users_list(2, test_person_with_favorite_list.pk)
        assert success
        favorite_users_list = test_person_repo.get_favorite_users_list(test_person_with_favorite_list.pk)
        assert len(favorite_users_list) == 0

    @pytest.mark.django_db
    def test_remove_from_favorite_users_list_when_wrong_id(
        self, test_person_with_favorite_list: Person, test_person_repo: PersonRepository
    ):
        success = test_person_repo.remove_from_favorite_users_list(123123132, test_person_with_favorite_list.pk)
        assert not success
        favorite_users_list = test_person_repo.get_favorite_users_list(test_person_with_favorite_list.pk)
        assert len(favorite_users_list) == 1

    @pytest.mark.django_db
    def test_get_person_by_id_when_not_exist(self, test_person_repo: PersonRepository):
        person = test_person_repo.get_person_by_id(123123)
        assert person is None

    @pytest.mark.django_db
    def test_format_favorite_users_list_empty(self, test_person: Person):
        res = format_favorite_users_list(test_person)
        assert res == "Favorite users:\n\nEmpty"

    @pytest.mark.django_db
    def test_format_favorite_users_list_one_item(self, test_person_with_favorite_list: Person):
        res = format_favorite_users_list(test_person_with_favorite_list)
        expected = "\n".join(
            ["Favorite users:\n"]
            + [
                f"{i+1} : Name: {p.name} - ID: {p.telegram_chat_id}"
                for i, p in enumerate(test_person_with_favorite_list.favorite_users_list.all().order_by("name"))
            ]
        )
        assert res == expected

    @pytest.mark.django_db
    def test_check_exists(self, test_person: Person, test_person_repo: PersonRepository):
        assert test_person_repo.check_exists(test_person.pk)
        assert not test_person_repo.check_exists(99999)

    @pytest.mark.django_db
    def test_get_person_id_by_telegram_id(self, test_person: Person, test_person_repo: PersonRepository):
        assert test_person_repo.get_person_id_by_telegram_id(test_person.telegram_chat_id) == test_person.pk
        assert test_person_repo.get_person_id_by_telegram_id(99999) is None

    @pytest.mark.django_db
    def test_update_password(self, test_person: Person, test_person_repo: PersonRepository):
        new_password = "1111"
        test_person_repo.update_password(test_person, new_password)
        assert check_password(new_password, test_person.password)

    @pytest.mark.django_db
    def test_get_person_by_telegram_id(self, test_person: Person, test_person_repo: PersonRepository):
        actual_person = test_person_repo.get_person_by_telegram_id(test_person.telegram_chat_id)
        assert actual_person is not None
        assert actual_person.pk == actual_person.pk

    @pytest.mark.django_db
    def test_test_check_phone_exists(self, test_person: Person, test_person_repo: PersonRepository):
        assert test_person_repo.check_phone_exists(test_person.phone)
        assert not test_person_repo.check_phone_exists("99999999999")
