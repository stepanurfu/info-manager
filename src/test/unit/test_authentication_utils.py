import time
from datetime import timedelta

import pytest

from app.internal.authentication.domain.services import AuthService
from app.internal.person.db.repositories import PersonRepository


class TestAuthenticationUtils:
    @pytest.mark.django_db
    def test_generate_access_and_refresh_tokens(self, test_person, test_auth_service: AuthService):
        test_auth_service.generate_jwt_tokens(test_person)
        assert test_person.refresh_tokens.count() == 1

    @pytest.mark.django_db
    def test_revoke_all_tokens(self, test_person, test_auth_service: AuthService):
        test_auth_service.generate_jwt_tokens(test_person)
        test_auth_service.generate_jwt_tokens(test_person)
        test_auth_service.revoke_all_tokens(test_person)
        for refresh_token in test_person.refresh_tokens.all():
            assert refresh_token.revoked

    @pytest.mark.django_db
    def test_revoke_token(self, test_person, test_auth_service: AuthService):
        jwt_tokens = test_auth_service.generate_jwt_tokens(test_person)
        test_auth_service.revoke_token(test_person.refresh_tokens.filter(jti=jwt_tokens.refresh_token).first())
        for refresh_token in test_person.refresh_tokens.all():
            assert refresh_token.revoked

    @pytest.mark.django_db
    def test_check_token_has_expired(self, test_person, test_auth_service: AuthService):
        access_token = test_person._generate_jwt_token(timedelta(days=1))
        assert not test_auth_service.check_token_has_expired(access_token)
        access_token = test_person._generate_jwt_token(timedelta(seconds=0))
        time.sleep(2)
        assert test_auth_service.check_token_has_expired(access_token)

    @pytest.mark.django_db
    def test_get_issued_token_by_raw_token(test, test_person, test_auth_service: AuthService):
        jwt_tokens = test_auth_service.generate_jwt_tokens(test_person)
        issued_token = test_auth_service.get_issued_token(jwt_tokens.refresh_token)
        assert issued_token is not None
        assert issued_token.jti == jwt_tokens.refresh_token

    @pytest.mark.django_db
    def test_get_user_by_phone(self, test_person, test_auth_service: AuthService):
        actual_user = test_auth_service.get_user_by_phone(test_person.phone)
        assert actual_user is not None
        assert actual_user.pk == test_person.pk

    @pytest.mark.django_db
    def test_get_person_by_phone(self, test_person, test_person_repo: PersonRepository):
        actual_person = test_person_repo.get_person_by_phone(test_person.phone)
        assert actual_person is not None
        assert actual_person.pk == test_person.pk
