from datetime import datetime, timedelta

import pytest
import pytz
from django.conf import settings
from django.utils.timezone import make_aware

from app.internal.common.utils import add_timezone, check_if_numberic


@pytest.mark.parametrize(
    "num_str,valid",
    [
        ("123", True),
        ("-123", True),
        ("123.123", True),
        ("-123.123", True),
        ("abc", False),
        ("123abc", False),
        ("123.abc", False),
        ("", False),
    ],
)
def test_check_if_numeric(num_str, valid):
    assert check_if_numberic(num_str) == valid


@pytest.mark.parametrize(
    "date",
    [datetime(2022, 1, 1)],
)
def test_add_timezone(date):
    assert abs(add_timezone(date) - make_aware(date, timezone=pytz.timezone(settings.TIME_ZONE))) < timedelta(seconds=1)
