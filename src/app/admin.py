from django.contrib import admin

from app.internal.authentication.presentation.admin import IssuedTokenAdmin
from app.internal.bank_account.presentation.admin import BankAccountAdmin
from app.internal.bank_card.presentation.admin import BankCardAdmin
from app.internal.bank_transaction.presentation.admin import BankTransactionAdmin
from app.internal.currency.presentation.admin import CurrencyAdmin
from app.internal.person.presentation.admin import PersonAdmin
from app.internal.post_card.presentation.admin import PostCardAdmin

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
