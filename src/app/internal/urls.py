from django.urls import path

from .authentication.app import configure_auth_api
from .bank_account.app import configure_bank_account_api
from .bank_card.app import configure_bank_card_api
from .bank_transaction.app import configure_bank_transaction_api
from .currency.app import configure_currency_api
from .person.app import configure_person_api
from .post_card.app import configure_post_card_api

urlpatterns = []
