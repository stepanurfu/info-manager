from ninja import NinjaAPI

from app.internal.authentication.http_jwt_auth import HTTJWTAuth

from .db.repositories import IssuedTokenRepository
from .domain.services import AuthService
from .presentation.handlers import AuthHandlers
from .presentation.routers import add_auth_router


def get_api():
    api = NinjaAPI(
        title="DT.EDU.BACKEND",
        version="1.0.0",
        auth=[HTTJWTAuth()],
    )
    api = configure_auth_api(api)
    return api


def configure_auth_api(api: NinjaAPI):
    issued_token_repo = IssuedTokenRepository()
    auth_service = AuthService(issued_token_repo=issued_token_repo)
    auth_handler = AuthHandlers(auth_service=auth_service)
    add_auth_router(api, auth_handler)
    return api
