from datetime import datetime, timedelta
from typing import List

import jwt
from django.conf import settings

from app.internal.common.exceptions import NotFoundException
from app.internal.person.db.models import Person

from ..db.models.issued_token import IssuedToken
from ..db.repositories import IIssuedTokenRepository
from .entities import JWTSchemaOut


class AuthService:
    def __init__(self, issued_token_repo: IIssuedTokenRepository):
        self._issued_token_repo = issued_token_repo

    def get_user_by_phone(self, phone: str) -> Person:
        return Person.objects.filter(phone=phone).first()

    def generate_jwt_tokens(self, person: Person) -> JWTSchemaOut:
        access_jwt_token = person._generate_jwt_token(timedelta(seconds=settings.JWT_ACCESS_TOKEN_EXP_TIME_IN_SEC))
        refresh_jwt_token = person._generate_jwt_token(timedelta(seconds=settings.JWT_REFRESH_TOKEN_EXP_TIME_IN_SEC))
        if not IssuedToken.objects.filter(jti=refresh_jwt_token).exists():
            IssuedToken.objects.create(jti=refresh_jwt_token, user=person)
        res = JWTSchemaOut(access_token=access_jwt_token, refresh_token=refresh_jwt_token)
        return res

    def revoke_all_tokens(self, person: Person):
        person.refresh_tokens.filter(revoked=False).update(revoked=True)

    def revoke_token(self, issued_token: IssuedToken):
        issued_token.revoked = True
        issued_token.save(update_fields=("revoked",))

    def get_issued_token(self, jti: str) -> IssuedToken:
        return self._issued_token_repo.get_issued_token(jti)

    def check_token_has_expired(self, raw_token) -> bool:
        try:
            payload = jwt.decode(raw_token, settings.SECRET_KEY, algorithms=["HS256"])
        except jwt.exceptions.ExpiredSignatureError:
            return True
        if "exp" not in payload:
            return True
        exp_time = datetime.fromtimestamp(payload["exp"])
        datetime_now = datetime.now()
        return exp_time < datetime_now
