from ninja import Schema
from pydantic import Field


class CredentialsSchema(Schema):
    phone: str = Field(max_length=15)
    password: str = Field(max_length=255)


class JWTSchemaOut(Schema):
    access_token: str = Field(max_length=255)
    refresh_token: str = Field(max_length=255)


class JWTRefreshSchema(Schema):
    refresh_token: str = Field(max_length=255)
