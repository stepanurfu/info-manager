from datetime import datetime

import jwt
from django.conf import settings
from django.contrib.auth import get_user_model
from django.http import HttpRequest
from ninja.security import APIKeyHeader, HttpBearer


class HTTJWTAuth(HttpBearer):
    def authenticate(self, request: HttpRequest, token):
        try:
            payload = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
            exp_time = datetime.fromtimestamp(payload["exp"])
            datetime_now = datetime.now()
            if exp_time < datetime_now:
                return None
            user = get_user_model().objects.get(id=payload["id"])
            request.user = user
        except:
            return None

        return token
