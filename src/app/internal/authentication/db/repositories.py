from ..db.models.issued_token import IssuedToken


class IIssuedTokenRepository:
    def get_issued_token(self, jti: str) -> IssuedToken:
        ...


class IssuedTokenRepository(IIssuedTokenRepository):
    def get_issued_token(self, jti: str) -> IssuedToken:
        if IssuedToken.objects.filter(pk=jti).exists():
            return IssuedToken.objects.get(pk=jti)
        return None
