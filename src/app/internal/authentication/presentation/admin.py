from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from app.internal.authentication.db.models.issued_token import IssuedToken


@admin.register(IssuedToken)
class IssuedTokenAdmin(admin.ModelAdmin):
    fields = ("jti", "user", "created_at", "revoked")
