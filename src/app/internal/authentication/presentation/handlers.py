import json

from django.contrib.auth.hashers import check_password
from django.http import HttpRequest, JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from ninja import Body, NinjaAPI

from app.internal.authentication.domain.entities import CredentialsSchema, JWTRefreshSchema
from app.internal.authentication.http_jwt_auth import HTTJWTAuth
from app.internal.common.exceptions import BadRequestException, UnauthorizedException

from ..domain.services import AuthService


@method_decorator(csrf_exempt, name="login")
class AuthHandlers:
    def __init__(self, auth_service: AuthService):
        self._auth_service = auth_service

    def login(self, request: HttpRequest, credentials: CredentialsSchema = Body(...)):
        user = self._auth_service.get_user_by_phone(credentials.phone)
        if not user or not check_password(credentials.password, user.password):
            raise UnauthorizedException("credentials are invalid")
        resp = self._auth_service.generate_jwt_tokens(user)
        return resp

    def refresh(self, request: HttpRequest, token_refresh_data: JWTRefreshSchema = Body(...)):
        body = json.loads(request.body.decode("utf-8"))
        if "refresh_token" not in body:
            return JsonResponse({"detail": "key refresh_token must be present"}, status=400)
        issued_token = self._auth_service.get_issued_token(token_refresh_data.refresh_token)
        if issued_token is None:
            raise BadRequestException("refresh token is invalid")
        if issued_token.revoked:
            self._auth_service.revoke_all_tokens(issued_token.user)
            raise BadRequestException("refresh token has been revoked")
        if self._auth_service.check_token_has_expired(token_refresh_data.refresh_token):
            raise BadRequestException("refresh token is expired")
        self._auth_service.revoke_token(issued_token)
        resp = self._auth_service.generate_jwt_tokens(issued_token.user)
        return resp
