from ninja import NinjaAPI, Router

from app.internal.common.responses import NotFoundResponse

from ..domain.entities import JWTSchemaOut
from ..presentation.handlers import AuthHandlers


def get_auth_router(auth_handlers: AuthHandlers):
    router = Router(tags=["auth"])

    router.add_api_operation(
        "/login",
        ["POST"],
        auth_handlers.login,
        response={200: JWTSchemaOut, 404: NotFoundResponse},
        auth=None,
    )

    router.add_api_operation(
        "/refresh",
        ["POST"],
        auth_handlers.refresh,
        response={200: JWTSchemaOut, 404: NotFoundResponse},
        auth=None,
    )

    return router


def add_auth_router(api: NinjaAPI, auth_handlers: AuthHandlers):
    auths_handler = get_auth_router(auth_handlers)
    api.add_router("", auths_handler)
