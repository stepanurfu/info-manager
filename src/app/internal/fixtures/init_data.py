def forwards_transaction_types(apps, schema_editor):
    # We get the model from the versioned app registry;
    # if we directly import it, it'll be the wrong version
    BankAccountTransactionType = apps.get_model("app", "BankAccountTransactionType")
    db_alias = schema_editor.connection.alias
    BankAccountTransactionType.objects.using(db_alias).bulk_create(
        [
            BankAccountTransactionType(name="withdraw"),
            BankAccountTransactionType(name="deposit"),
        ]
    )


def reverse_transaction_types(apps, schema_editor):
    BankAccountTransactionType = apps.get_model("app", "BankAccountTransactionType")
    db_alias = schema_editor.connection.alias
    BankAccountTransactionType.objects.using(db_alias).filter(name="withdraw").delete()
    BankAccountTransactionType.objects.using(db_alias).filter(name="deposit").delete()


def forwards_currencies(apps, schema_editor):
    # We get the model from the versioned app registry;
    # if we directly import it, it'll be the wrong version
    Currency = apps.get_model("app", "Currency")
    db_alias = schema_editor.connection.alias
    Currency.objects.using(db_alias).bulk_create(
        [
            Currency(name="RUB"),
        ]
    )


def reverse_currencies(apps, schema_editor):
    Currency = apps.get_model("app", "Currency")
    db_alias = schema_editor.connection.alias
    Currency.objects.using(db_alias).filter(name="RUB").delete()
