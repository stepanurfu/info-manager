from ninja import NinjaAPI

from app.internal.authentication.http_jwt_auth import HTTJWTAuth

from .db.repositories import CurrencyRepository
from .domain.services import CurrencyService
from .presentation.handlers import CurrencyHandlers
from .presentation.routers import add_currency_router


def get_api():
    api = NinjaAPI(
        title="DT.EDU.BACKEND",
        version="1.0.0",
        auth=[HTTJWTAuth()],
    )
    api = configure_currency_api(api)
    return api


def configure_currency_api(api: NinjaAPI):
    currency_repo = CurrencyRepository()
    currency_service = CurrencyService(currency_repo=currency_repo)
    currency_handler = CurrencyHandlers(currency_service=currency_service)
    add_currency_router(api, currency_handler)
    return api
