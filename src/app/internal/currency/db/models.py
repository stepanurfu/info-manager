from django.db import models

from app.internal.common.models import CommonModel


class Currency(CommonModel):
    name = models.CharField(max_length=20)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = "Currency"
        verbose_name_plural = "Currencies"
