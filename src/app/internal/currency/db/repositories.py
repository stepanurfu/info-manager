from typing import List, Optional

from ..db.models import Currency
from ..domain.entities import CurrencyIn


class ICurrencyRepository:
    def get_currency_by_id(self, currency_id: int) -> Currency:
        ...

    def get_currencies(self) -> list[Currency]:
        ...

    def add_currency(self, currency_data: CurrencyIn) -> Currency:
        ...


class CurrencyRepository(ICurrencyRepository):
    def get_currency_by_id(self, currency_id: int) -> Optional[Currency]:
        currency: Optional[Currency] = Currency.objects.filter(pk=currency_id).first()
        return currency

    def get_currencies(self) -> List[Currency]:
        return [Currency.objects.all()]

    def add_currency(self, currency_data: CurrencyIn) -> Currency:
        currency = Currency.objects.create(**currency_data.dict())
        return currency
