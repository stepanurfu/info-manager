from typing import List

from ninja import Body, Path

from app.internal.common.responses import SuccessResponse

from ..domain.entities import CurrencyIn, CurrencyOut
from ..domain.services import CurrencyService


class CurrencyHandlers:
    def __init__(self, currency_service: CurrencyService):
        self._currency_service = currency_service

    def get_currency_by_id(self, request, currency_id: int = Path(...)) -> CurrencyOut:
        currency = self._currency_service.get_currency_by_id(currency_id=currency_id)
        return currency

    def get_currencies(self, request) -> List[CurrencyOut]:
        return self._currency_service.get_currencies()
