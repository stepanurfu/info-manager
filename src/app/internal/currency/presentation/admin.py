from django.contrib import admin

from ..db.models import Currency


@admin.register(Currency)
class CurrencyAdmin(admin.ModelAdmin):
    fields = ("name",)
