from ninja import NinjaAPI, Router

from app.internal.common.responses import NotFoundResponse

from ..domain.entities import CurrencyOut
from ..presentation.handlers import CurrencyHandlers


def get_currency_router(currency_handlers: CurrencyHandlers):
    router = Router(tags=["currency"])

    router.add_api_operation(
        "/{int:currency_id}",
        ["GET"],
        currency_handlers.get_currency_by_id,
        response={200: CurrencyOut, 404: NotFoundResponse},
    )

    return router


def add_currency_router(api: NinjaAPI, currency_handlers: CurrencyHandlers):
    persons_handler = get_currency_router(currency_handlers)
    api.add_router("/currencies", persons_handler)
