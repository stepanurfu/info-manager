from ninja import Schema
from pydantic import Field


class CurrencySchema(Schema):
    name: str = Field(max_length=255)


class CurrencyOut(CurrencySchema):
    id: int
    ...


class CurrencyIn(CurrencySchema):
    ...
