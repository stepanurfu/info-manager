from typing import List
from uuid import UUID

from app.internal.common.exceptions import NotFoundException

from ..db.repositories import ICurrencyRepository
from .entities import CurrencyIn, CurrencyOut


class CurrencyService:
    def __init__(self, currency_repo: ICurrencyRepository):
        self._currency_repo = currency_repo

    def get_currency_by_id(self, currency_id: int) -> CurrencyOut:
        currency = self._currency_repo.get_currency_by_id(currency_id=currency_id)
        if currency is None:
            raise NotFoundException("Currency", currency_id)
        return CurrencyOut.from_orm(currency)

    def get_currencies(self) -> List[CurrencyOut]:
        currencys = self._currency_repo.get_currencies()
        return [CurrencyOut.from_orm(p) for p in currencys]

    def add_currency(self, currency_data: CurrencyIn) -> bool:
        return self._currency_repo.add_currency(currency_data)
