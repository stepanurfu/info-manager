from ninja import NinjaAPI, Router

from app.internal.common.responses import NotFoundResponse

from ..domain.entities import BankTransactionOut
from ..presentation.handlers import BankTransactionHandlers


def get_bank_transaction_router(bank_transaction_handlers: BankTransactionHandlers):
    router = Router(tags=["bank_transactions"])

    router.add_api_operation(
        "/{int:bank_transaction_id}",
        ["GET"],
        bank_transaction_handlers.get_bank_transaction_by_id,
        response={200: BankTransactionOut, 404: NotFoundResponse},
    )

    return router


def add_bank_transaction_router(api: NinjaAPI, bank_transaction_handlers: BankTransactionHandlers):
    persons_handler = get_bank_transaction_router(bank_transaction_handlers)
    api.add_router("/bank_transactions", persons_handler)
