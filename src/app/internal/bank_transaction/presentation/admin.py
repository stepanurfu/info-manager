from django.contrib import admin

from ..db.models import BankTransaction


@admin.register(BankTransaction)
class BankTransactionAdmin(admin.ModelAdmin):
    fields = ("datetime_at", "bank_card_from", "bank_card_to", "amount", "postcard")
