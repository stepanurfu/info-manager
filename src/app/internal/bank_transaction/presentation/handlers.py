from typing import List

from ninja import Body, Path

from app.internal.common.responses import SuccessResponse

from ..domain.entities import BankTransactionIn, BankTransactionOut
from ..domain.services import BankTransactionService


class BankTransactionHandlers:
    def __init__(self, bank_transaction_service: BankTransactionService):
        self._bank_transaction_service = bank_transaction_service

    def get_bank_transaction_by_id(self, request, bank_transaction_id: int = Path(...)) -> BankTransactionOut:
        bank_transaction = self._bank_transaction_service.get_bank_transaction_by_id(
            bank_transaction_id=bank_transaction_id
        )
        return bank_transaction

    def get_bank_transactions(self, request) -> List[BankTransactionOut]:
        return self._bank_transaction_service.get_bank_transactions()
