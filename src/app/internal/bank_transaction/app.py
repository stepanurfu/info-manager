from ninja import NinjaAPI

from app.internal.authentication.http_jwt_auth import HTTJWTAuth
from app.internal.bank_account.db.repositories import BankAccountRepository
from app.internal.bank_card.db.repositories import BankCardRepository

from .db.repositories import BankTransactionRepository
from .domain.services import BankTransactionService
from .presentation.handlers import BankTransactionHandlers
from .presentation.routers import add_bank_transaction_router


def get_api():
    api = NinjaAPI(
        title="DT.EDU.BACKEND",
        version="1.0.0",
        auth=[HTTJWTAuth()],
    )
    api = configure_bank_transaction_api(api)
    return api


def configure_bank_transaction_api(api: NinjaAPI):
    bank_transaction_repo = BankTransactionRepository()
    bank_account_repo = BankAccountRepository()
    bank_card_repo = BankCardRepository()
    bank_transaction_service = BankTransactionService(
        bank_transaction_repo=bank_transaction_repo,
        bank_account_repo=bank_account_repo,
        bank_card_repo=bank_card_repo,
    )
    bank_transaction_handler = BankTransactionHandlers(bank_transaction_service=bank_transaction_service)
    add_bank_transaction_router(api, bank_transaction_handler)
    return api
