from datetime import date
from decimal import Decimal

from ninja import Schema
from pydantic import Field

from app.internal.bank_card.domain.entities import BankCardSchema


class BankTransactionSchema(Schema):
    datetime_at: date
    bank_card_from: BankCardSchema
    bank_card_to: BankCardSchema
    amount: Decimal = Field(0)


class BankTransactionOut(BankTransactionSchema):
    id: int


class BankTransactionIn(BankTransactionSchema):
    ...
