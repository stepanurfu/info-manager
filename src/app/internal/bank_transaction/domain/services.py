import datetime
from typing import List

from app.internal.bank_account.db.repositories import IBankAccountRepository
from app.internal.bank_card.db.repositories import IBankCardRepository
from app.internal.common.exceptions import NotFoundException
from app.internal.common.utils import BankTransactionAccounting
from app.internal.person.db.models import Person

from ..db.repositories import IBankTransactionRepository
from .entities import BankTransactionIn, BankTransactionOut


class BankTransactionService:
    def __init__(
        self,
        bank_transaction_repo: IBankTransactionRepository,
        bank_account_repo: IBankAccountRepository,
        bank_card_repo: IBankCardRepository,
    ):
        self._bank_transaction_repo = bank_transaction_repo
        self._bank_account_repo = bank_account_repo
        self._bank_card_repo = bank_card_repo

    def get_bank_transaction_by_id(self, bank_transaction_id: int) -> BankTransactionOut:
        bank_transaction = self._bank_transaction_repo.get_bank_transaction_by_id(
            bank_transaction_id=bank_transaction_id
        )
        if bank_transaction is None:
            raise NotFoundException("BankTransaction", bank_transaction_id)
        return BankTransactionOut.from_orm(bank_transaction)

    def get_bank_transactions(self) -> List[BankTransactionOut]:
        bank_transactions = self._bank_transaction_repo.get_currencies()
        return [BankTransactionOut.from_orm(bc) for bc in bank_transactions]

    def add_BankTransaction(self, bank_transaction_data: BankTransactionIn) -> bool:
        return self._bank_transaction_repo.add_bank_transaction(bank_transaction_data)

    def get_transaction_info_by_card_number(
        self, person: Person, bank_card_number: str, start_date: datetime.date = None, end_date: datetime.date = None
    ) -> BankTransactionAccounting:
        card = self._bank_card_repo.get_bank_card_by_number(person, bank_card_number)
        if not card:
            return None
        return self._bank_transaction_repo.get_transaction_info_by_card(card, start_date, end_date)

    def get_transaction_info_by_account_number(
        self, person: Person, bank_account_number: str, start_date: datetime.date = None, end_date: datetime.date = None
    ) -> list[BankTransactionAccounting]:
        bank_account = self._bank_account_repo.get_bank_account_by_number(person, bank_account_number)
        if not bank_account:
            return None

        bank_transaction_accountings_per_card = []
        for card in bank_account.bank_cards.all():
            bank_transaction_accounting = self._bank_transaction_repo.get_transaction_info_by_card(
                card, start_date, end_date
            )
            bank_transaction_accountings_per_card.append(bank_transaction_accounting)

        return bank_transaction_accountings_per_card
