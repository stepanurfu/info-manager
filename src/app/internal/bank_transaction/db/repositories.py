import datetime
import decimal
from typing import List, Optional

import django.utils.timezone
from django.db import transaction
from django.db.models import F, Q

from app.internal.bank_card.db.models import BankCard
from app.internal.common.utils import BankTransactionAccounting, BankTransactionInfo, add_timezone
from app.internal.person.db.models import Person
from app.internal.post_card.db.models import PostCard

from ..db.models import BankTransaction
from ..domain.entities import BankTransactionIn


class IBankTransactionRepository:
    def get_bank_transaction_by_id(self, bank_transaction_id: int) -> BankTransaction:
        ...

    def get_bank_transactions(self) -> list[BankTransaction]:
        ...

    def add_bank_transaction(self, bank_transaction_data: BankTransactionIn) -> BankTransaction:
        ...

    def get_transaction_info_by_card(
        self, card: BankCard, start_date: datetime.date = None, end_date: datetime.date = None
    ) -> BankTransactionAccounting:
        ...

    def get_all_transfer_relative_names(self, person: Person):
        ...

    def transfer_by_card(
        self, person: Person, bank_card_number_from: str, bank_card_number_to: str, amount: float
    ) -> BankTransaction:
        ...

    def attach_post_card(self, bank_transaction: BankTransaction, post_card: PostCard) -> bool:
        ...

    def get_new_transactions(self, person: Person) -> List[BankTransactionAccounting]:
        ...


class BankTransactionRepository(IBankTransactionRepository):
    def get_bank_transaction_by_id(self, bank_transaction_id: int) -> Optional[BankTransaction]:
        bank_transaction: Optional[BankTransaction] = BankTransaction.objects.filter(pk=bank_transaction_id).first()
        return bank_transaction

    def get_bank_transactions(self) -> List[BankTransaction]:
        return [BankTransaction.objects.all()]

    def add_bank_transaction(self, bank_transaction_data: BankTransactionIn) -> BankTransaction:
        bank_transaction = BankTransaction.objects.create(**bank_transaction_data.dict())
        return bank_transaction

    def transfer_by_card(
        self, person: Person, bank_card_number_from: str, bank_card_number_to: str, amount: float
    ) -> BankTransaction:
        if bank_card_number_from == bank_card_number_to:
            return None
        amount = decimal.Decimal(amount)
        if (
            not BankCard.objects.filter(number=bank_card_number_from).exists()
            or not BankCard.objects.filter(number=bank_card_number_to).exists()
        ):
            return None
        bank_transaction = None
        with transaction.atomic():
            card_from = BankCard.objects.select_for_update().filter(number=bank_card_number_from).first()
            if card_from.balance < amount:
                return None
            card_to = BankCard.objects.select_for_update().filter(number=bank_card_number_to).first()
            card_from.balance = F("balance") - amount
            card_to.balance = F("balance") + amount
            card_from.save(update_fields=("balance",))
            card_to.save(update_fields=("balance",))
            bank_transaction = BankTransaction.objects.create(
                bank_card_from=card_from, bank_card_to=card_to, amount=amount
            )
        return bank_transaction

    def _get_transaction_info_by_card(
        self, card: BankCard, start_datetime: datetime.datetime = None, end_datetime: datetime.datetime = None
    ) -> BankTransactionAccounting:
        all_transactions = (
            BankTransaction.objects.filter(
                (Q(bank_card_from=card) | Q(bank_card_to=card)) & Q(datetime_at__range=[start_datetime, end_datetime])
            )
            .order_by("datetime_at")
            .values("datetime_at", "bank_card_from__number", "bank_card_to__number", "amount", "postcard")
            .all()
        )

        bank_transaction_infos = []
        for transaction_dict in all_transactions:
            url = None
            postcard_id = None
            if transaction_dict["postcard"] is not None:
                postcard = PostCard.objects.get(pk=transaction_dict["postcard"])
                if postcard.content:
                    url = postcard.content.url
                postcard_id = postcard.pk
            bank_transaction_infos.append(
                BankTransactionInfo(
                    transaction_dict["datetime_at"],
                    transaction_dict["bank_card_from__number"],
                    transaction_dict["bank_card_to__number"],
                    transaction_dict["amount"],
                    transaction_dict["bank_card_to__number"] == card.number,
                    postcard_id,
                    url,
                )
            )

        return BankTransactionAccounting(card.number, card.balance, bank_transaction_infos)

    def get_transaction_info_by_card(
        self, card: BankCard, start_date: datetime.date = None, end_date: datetime.date = None
    ) -> BankTransactionAccounting:
        if not start_date:
            start_date = datetime.datetime(1970, 1, 1)
        start_datetime = datetime.datetime.combine(start_date, datetime.time.min)
        if not end_date:
            end_date = datetime.date.today()
        end_datetime = datetime.datetime.combine(end_date, datetime.time.max)
        start_datetime = add_timezone(start_datetime)
        end_datetime = add_timezone(end_datetime)
        return self._get_transaction_info_by_card(card, start_datetime, end_datetime)

    def get_all_transfer_relative_names(self, person: Person):
        related_names_from = (
            BankTransaction.objects.filter(bank_card_from__bank_account__customers=person)
            .values_list("bank_card_to__bank_account__customers__name", flat=True)
            .distinct()
        )
        related_names_to = (
            BankTransaction.objects.filter(bank_card_to__bank_account__customers=person)
            .values_list("bank_card_from__bank_account__customers__name", flat=True)
            .distinct()
        )
        return related_names_from.union(related_names_to)

    def attach_post_card(self, bank_transaction: BankTransaction, postcard: PostCard) -> bool:
        bank_transaction.postcard = postcard
        bank_transaction.save(update_fields=("postcard",))
        return True

    def get_new_transactions(self, person: Person) -> List[BankTransactionAccounting]:
        end_datetime = datetime.datetime.combine(datetime.date.today(), datetime.time.max)
        end_datetime = add_timezone(end_datetime)
        res = []
        for bank_account in person.bank_accounts.all():
            for bank_card in bank_account.bank_cards.all():
                accounting = self._get_transaction_info_by_card(bank_card, person.last_transaction_check, end_datetime)
                res.append(accounting)
        return res
