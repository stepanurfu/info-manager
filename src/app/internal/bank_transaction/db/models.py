from datetime import datetime

from django.db import models
from django.utils import timezone

from app.internal.bank_card.db.models import BankCard
from app.internal.common.models import CommonModel
from app.internal.post_card.db.models import PostCard


class BankTransaction(CommonModel):
    datetime_at = models.DateTimeField(default=timezone.now)
    bank_card_from = models.ForeignKey(BankCard, on_delete=models.CASCADE, related_name="transactions_from")
    bank_card_to = models.ForeignKey(BankCard, on_delete=models.CASCADE, related_name="transactions_to")
    amount = models.DecimalField(default=0, max_digits=19, decimal_places=4)
    postcard = models.ForeignKey(PostCard, on_delete=models.CASCADE, related_name="transactions", blank=True, null=True)

    def __str__(self):
        return f"{self.bank_card_from} -> {self.bank_card_to} {self.amount}"

    class Meta:
        verbose_name = "Bank Transaction"
        verbose_name_plural = "Bank Transactions"
        indexes = [
            models.Index(fields=["datetime_at"]),
        ]
