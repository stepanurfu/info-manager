from typing import List, Optional
from uuid import UUID

from django.core.files.base import ContentFile

from app.internal.common.exceptions import AlreadyExistException, NotFoundException
from app.internal.person.db.models import Person

from ..db.models import PostCard
from ..domain.entities import PostCardIn, PostCardOut


class IPostCardRepository:
    def get_post_card_by_id(self, post_card_id: int) -> PostCard:
        ...

    def get_post_cards(self) -> list[PostCard]:
        ...

    def add_post_card(self, post_card_data: PostCardIn) -> PostCard:
        ...

    def update_post_card(self, post_card_id: int, post_card_data: PostCardIn) -> PostCard:
        ...


class PostCardRepository(IPostCardRepository):
    def get_post_card_by_id(self, post_card_id: int) -> Optional[PostCard]:
        post_card: Optional[PostCard] = PostCard.objects.filter(pk=post_card_id).first()
        return post_card

    def get_post_cards(self) -> List[PostCard]:
        return [PostCard.objects.all()]

    def add_post_card(self, post_card_data: PostCardIn, file_name=None) -> PostCard:
        post_card_data_dict = post_card_data.dict()
        post_card_data_dict["content"] = ContentFile(post_card_data.content, file_name)
        post_card = PostCard.objects.create(**post_card_data_dict)
        return post_card

    def update_post_card(self, post_card_id: int, post_card_data: PostCardIn) -> PostCard:
        post_card_data_dict = post_card_data.dict()
        post_card, created = PostCard.objects.get_or_create(pk=post_card_id)
        post_card.update(**post_card_data_dict)
        return post_card
