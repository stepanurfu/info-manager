from django.db import models

from app.internal.common.models import CommonModel


class PostCard(CommonModel):
    content = models.FileField()

    def __str__(self):
        return f"{self.pk}"

    class Meta:
        verbose_name = "Post Card"
        verbose_name_plural = "Post Card"
