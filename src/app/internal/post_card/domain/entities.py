from certifi import contents
from ninja import Schema
from pydantic import Field

from app.internal.common.schemas import PkSchema


class PostCardSchema(Schema):
    content: bytes = Field(b"")


class PostCardOut(Schema):
    id: int
    path: str

    class Config:
        fields = {"id": "pk"}


class PostCardIn(PostCardSchema):
    pass
