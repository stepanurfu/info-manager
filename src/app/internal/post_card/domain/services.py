from typing import List

from django.conf import settings

from app.internal.common.exceptions import NotFoundException

from ..db.repositories import IPostCardRepository
from .entities import PostCardIn, PostCardOut


class PostCardService:
    def __init__(self, post_card_repo: IPostCardRepository):
        self._post_card_repo = post_card_repo

    def get_post_card_by_id(self, post_card_id: int) -> PostCardOut:
        post_card = self._post_card_repo.get_post_card_by_id(post_card_id=post_card_id)
        if post_card is None:
            raise NotFoundException("PostCard", post_card_id)
        path = ""
        if post_card.content:
            path = post_card.content.url
        return PostCardOut(pk=post_card.pk, path=path)

    def add_post_card(self, post_card_data: PostCardIn) -> bool:
        return self._post_card_repo.add_post_card(post_card_data)
