from typing import List

from ninja import Body, Path

from app.internal.common.responses import SuccessResponse

from ..domain.entities import PostCardIn, PostCardOut
from ..domain.services import PostCardService


class PostCardHandlers:
    def __init__(self, post_card_service: PostCardService):
        self._post_card_service = post_card_service

    def get_post_card_by_id(self, request, post_card_id: int = Path(...)) -> PostCardOut:
        post_card = self._post_card_service.get_post_card_by_id(post_card_id=post_card_id)
        return post_card
