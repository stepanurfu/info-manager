from ninja import NinjaAPI, Router

from app.internal.common.responses import NotFoundResponse

from ..domain.entities import PostCardOut
from ..presentation.handlers import PostCardHandlers


def get_post_card_router(post_card_handlers: PostCardHandlers):
    router = Router(tags=["post_cards"])

    router.add_api_operation(
        "/{int:post_card_id}",
        ["GET"],
        post_card_handlers.get_post_card_by_id,
        response={200: PostCardOut, 404: NotFoundResponse},
    )

    return router


def add_post_card_router(api: NinjaAPI, post_card_handlers: PostCardHandlers):
    persons_handler = get_post_card_router(post_card_handlers)
    api.add_router("/post_cards", persons_handler)
