from django.contrib import admin

from ..db.models import PostCard


@admin.register(PostCard)
class PostCardAdmin(admin.ModelAdmin):
    fields = ("content",)
