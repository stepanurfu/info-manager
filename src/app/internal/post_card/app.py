from ninja import NinjaAPI

from app.internal.authentication.http_jwt_auth import HTTJWTAuth

from .db.repositories import PostCardRepository
from .domain.services import PostCardService
from .presentation.handlers import PostCardHandlers
from .presentation.routers import add_post_card_router


def get_api():
    api = NinjaAPI(
        title="DT.EDU.BACKEND",
        version="1.0.0",
        auth=[HTTJWTAuth()],
    )
    api = configure_post_card_api(api)
    return api


def configure_post_card_api(api: NinjaAPI):
    post_card_repo = PostCardRepository()
    post_card_service = PostCardService(post_card_repo=post_card_repo)
    post_card_handler = PostCardHandlers(post_card_service=post_card_service)
    add_post_card_router(api, post_card_handler)
    return api
