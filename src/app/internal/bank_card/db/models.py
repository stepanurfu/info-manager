from django.db import models

from app.internal.bank_account.db.models import BankAccount
from app.internal.common.models import CommonModel


class BankCard(CommonModel):
    number = models.CharField(max_length=19, unique=True)
    code = models.CharField(max_length=3)
    bank_account = models.ForeignKey(BankAccount, on_delete=models.CASCADE, related_name="bank_cards")
    balance = models.DecimalField(default=0, max_digits=19, decimal_places=4)

    def __str__(self):
        return f"{self.number}"

    class Meta:
        verbose_name = "Bank Card"
        verbose_name_plural = "Bank Card"
