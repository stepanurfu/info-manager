from typing import List, Optional
from uuid import UUID

from django.db import transaction
from django.db.models import F, Prefetch, Q, Sum

from app.internal.bank_account.db.models import BankAccount
from app.internal.common.exceptions import AlreadyExistException, NotFoundException
from app.internal.person.db.models import Person

from ..db.models import BankCard
from ..domain.entities import BankCardIn, BankCardOut


class IBankCardRepository:
    def get_bank_card_by_id(self, bank_card_id: int) -> BankCard:
        ...

    def get_bank_cards(self) -> list[BankCard]:
        ...

    def add_bank_card(self, bank_card_data: BankCardIn) -> BankCard:
        ...

    def update_bank_card(self, bank_card_id: int, bank_card_data: BankCardIn) -> BankCard:
        ...

    def get_bank_card_by_number(self, person: Person, bank_card_number: str) -> BankCard:
        ...

    def get_person_cards(self, person: Person) -> dict[str, str]:
        ...


class BankCardRepository(IBankCardRepository):
    def get_bank_card_by_id(self, bank_card_id: int) -> Optional[BankCard]:
        bank_card: Optional[BankCard] = BankCard.objects.filter(pk=bank_card_id).first()
        return bank_card

    def get_bank_cards(self) -> List[BankCard]:
        return [BankCard.objects.all()]

    def add_bank_card(self, bank_card_data: BankCardIn) -> BankCard:
        bank_card_data_dict = bank_card_data.dict()
        bank_account_id = bank_card_data_dict.pop("bank_account")
        if not BankAccount.objects.filter(pk=bank_account_id).exists():
            raise NotFoundException("BankAccount", bank_account_id)
        if BankCard.objects.filter(number=bank_card_data.number).exists():
            raise AlreadyExistException("BankCard", "number")
        bank_card = BankCard.objects.create(**bank_card_data_dict, bank_account_id=bank_account_id)
        return bank_card

    def update_bank_card(self, bank_card_id: int, bank_card_data: BankCardIn) -> BankCard:
        bank_card_data_dict = bank_card_data.dict()
        bank_account_id = bank_card_data_dict.pop("bank_account")
        if not BankAccount.objects.filter(pk=bank_account_id).exists():
            raise NotFoundException("BankAccount", bank_account_id)
        if BankCard.objects.filter(number=bank_card_data.number).exists():
            raise AlreadyExistException("BankCard", "number")
        bank_card, created = BankCard.objects.get_or_create(pk=bank_card_id)
        bank_card_data_dict["bank_account_id"] = bank_account_id
        bank_card.update(**bank_card_data_dict)
        return bank_card

    def get_bank_card_by_number(self, person: Person, bank_card_number: str) -> BankCard:
        if (
            BankCard.objects.select_related("bank_account")
            .filter(Q(bank_account__customers=person) & Q(number=bank_card_number))
            .exists()
        ):
            return BankCard.objects.filter(number=bank_card_number).first()
        return None

    def get_person_cards(self, person: Person) -> dict[str, str]:
        res = dict()
        for bank_account in person.bank_accounts.all():
            res[bank_account.number] = [card["number"] for card in bank_account.bank_cards.values("number").all()]
        return res
