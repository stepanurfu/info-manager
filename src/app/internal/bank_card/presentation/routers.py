from ninja import NinjaAPI, Router

from app.internal.common.responses import NotFoundResponse

from ..domain.entities import BankCardOut
from ..presentation.handlers import BankCardHandlers


def get_bank_card_router(bank_card_handlers: BankCardHandlers):
    router = Router(tags=["bank_cards"])

    router.add_api_operation(
        "/{int:bank_card_id}",
        ["GET"],
        bank_card_handlers.get_bank_card_by_id,
        response={200: BankCardOut, 404: NotFoundResponse},
    )

    router.add_api_operation(
        "",
        ["POST"],
        bank_card_handlers.add_bank_card,
        response={200: BankCardOut, 404: NotFoundResponse},
    )

    router.add_api_operation(
        "/{int:bank_card_id}",
        ["PUT"],
        bank_card_handlers.update_bank_card,
        response={200: BankCardOut, 404: NotFoundResponse},
    )

    return router


def add_bank_card_router(api: NinjaAPI, bank_card_handlers: BankCardHandlers):
    persons_handler = get_bank_card_router(bank_card_handlers)
    api.add_router("/bank_cards", persons_handler)
