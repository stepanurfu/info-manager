from typing import List

from ninja import Body, Path

from app.internal.common.responses import SuccessResponse

from ..domain.entities import BankCardIn, BankCardOut
from ..domain.services import BankCardService


class BankCardHandlers:
    def __init__(self, bank_card_service: BankCardService):
        self._bank_card_service = bank_card_service

    def get_bank_card_by_id(self, request, bank_card_id: int = Path(...)) -> BankCardOut:
        bank_card = self._bank_card_service.get_bank_card_by_id(bank_card_id=bank_card_id)
        return bank_card

    def get_bank_cards(self, request) -> List[BankCardOut]:
        return self._bank_card_service.get_bank_cards()

    def add_bank_card(self, request, bank_card_data: BankCardIn = Body(...)) -> BankCardOut:
        return self._bank_card_service.add_bank_card(bank_card_data)

    def update_bank_card(
        self, request, bank_card_id: int = Path(...), bank_card_data: BankCardIn = Body(...)
    ) -> BankCardOut:
        return self._bank_card_service.update_bank_card(bank_card_id, bank_card_data)
