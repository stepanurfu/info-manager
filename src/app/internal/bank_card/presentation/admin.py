from django.contrib import admin

from ..db.models import BankCard


@admin.register(BankCard)
class BankCardAdmin(admin.ModelAdmin):
    fields = ("number", "code", "bank_account", "balance")
