from ninja import Schema
from pydantic import Field

from app.internal.common.schemas import PkSchema


class BankCardSchema(Schema):
    number: str = Field(max_length=19)
    code: str = Field(max_length=3)
    balance: int = Field(0)


class BankCardOut(BankCardSchema):
    id: int
    bank_account: PkSchema

    class Config:
        fields = {"id": "pk"}


class BankCardIn(BankCardSchema):
    bank_account: int
