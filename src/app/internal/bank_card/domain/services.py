from typing import List

from app.internal.common.exceptions import NotFoundException

from ..db.repositories import IBankCardRepository
from .entities import BankCardIn, BankCardOut


class BankCardService:
    def __init__(self, bank_card_repo: IBankCardRepository):
        self._bank_card_repo = bank_card_repo

    def get_bank_card_by_id(self, bank_card_id: int) -> BankCardOut:
        bank_card = self._bank_card_repo.get_bank_card_by_id(bank_card_id=bank_card_id)
        if bank_card is None:
            raise NotFoundException("BankCard", bank_card_id)
        return BankCardOut.from_orm(bank_card)

    def get_bank_cards(self) -> List[BankCardOut]:
        bank_cards = self._bank_card_repo.get_currencies()
        return [BankCardOut.from_orm(bc) for bc in bank_cards]

    def add_bank_card(self, bank_card_data: BankCardIn) -> bool:
        return self._bank_card_repo.add_bank_card(bank_card_data)

    def update_bank_card(self, bank_card_id: int, bank_card_data: BankCardIn) -> BankCardOut:
        return self._bank_card_repo.update_bank_card(bank_card_id, bank_card_data)
