from ninja import NinjaAPI

from app.internal.authentication.http_jwt_auth import HTTJWTAuth

from .db.repositories import BankCardRepository
from .domain.services import BankCardService
from .presentation.handlers import BankCardHandlers
from .presentation.routers import add_bank_card_router


def get_api():
    api = NinjaAPI(
        title="DT.EDU.BACKEND",
        version="1.0.0",
        auth=[HTTJWTAuth()],
    )
    api = configure_bank_card_api(api)
    return api


def configure_bank_card_api(api: NinjaAPI):
    bank_card_repo = BankCardRepository()
    bank_card_service = BankCardService(bank_card_repo=bank_card_repo)
    bank_card_handler = BankCardHandlers(bank_card_service=bank_card_service)
    add_bank_card_router(api, bank_card_handler)
    return api
