from app.internal.person.db.models import Person


def format_favorite_users_list(person: Person) -> str:
    parts_title = ["Favorite users:\n"]
    parts = [
        f"{i+1} : Name: {p['name']} - ID: {p['telegram_chat_id']}"
        for i, p in enumerate(person.favorite_users_list.order_by("name").values("name", "telegram_chat_id").all())
    ]
    if not len(parts):
        parts = ["Empty"]
    return "\n".join(parts_title + parts)
