from django.contrib import admin

from ..db.models import Person


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    fields = (
        "name",
        "date_of_birth",
        "phone",
        "password",
        "telegram_chat_id",
        "last_transaction_check",
        "favorite_users_list",
    )
    filter_horizontal = ("favorite_users_list",)
