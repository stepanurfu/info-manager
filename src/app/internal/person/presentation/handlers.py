from typing import List

from ninja import Body, Path

from app.internal.common.responses import SuccessResponse
from app.internal.common.schemas import IdSchema

from ..domain.entities import PersonOut
from ..domain.services import PersonService


class PersonHandlers:
    def __init__(self, person_service: PersonService):
        self._person_service = person_service

    def get_person_by_id(self, request, person_id: int = Path(...)) -> PersonOut:
        person = self._person_service.get_person_by_id(person_id=person_id)
        return person

    def add_person_to_favorite_users(
        self, request, person_id: int = Path(...), person_to_add: IdSchema = Body(...)
    ) -> SuccessResponse:
        success = self._person_service.add_to_favorite_users_list(person_to_add.id, person_id)
        return SuccessResponse(success={"success": success})

    def remove_person_from_favorite_users(
        self, request, person_id: int = Path(...), id_to_delete: int = Path(...)
    ) -> SuccessResponse:
        success = self._person_service.remove_from_favorite_users_list(id_to_delete, person_id)
        return SuccessResponse(success={"success": success})

    def get_favorite_users(self, request, person_id: int = Path(...)) -> List[PersonOut]:
        people = self._person_service.get_favorite_users_list(person_id)
        return people

    def get_my_info(self, request):
        user = request.user
        person = self._person_service.get_person_by_phone(user.phone)
        return person
