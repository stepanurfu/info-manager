from typing import List

from ninja import NinjaAPI, Router

from app.internal.common.responses import ErrorResponse, NotFoundResponse, SuccessResponse

from ..domain.entities import PersonOut
from ..presentation.handlers import PersonHandlers


def get_person_router(person_handlers: PersonHandlers):
    router = Router(tags=["person"])

    router.add_api_operation(
        "/{int:person_id}/favorite_users_list",
        ["GET"],
        person_handlers.get_favorite_users,
        response={200: List[PersonOut], 400: ErrorResponse, 404: NotFoundResponse},
    )

    router.add_api_operation(
        "/{int:person_id}/favorite_users_list",
        ["POST"],
        person_handlers.add_person_to_favorite_users,
        response={200: SuccessResponse, 400: ErrorResponse, 404: NotFoundResponse},
    )

    router.add_api_operation(
        "/{int:person_id}/favorite_users_list/{int:id_to_delete}",
        ["DELETE"],
        person_handlers.remove_person_from_favorite_users,
        response={200: SuccessResponse, 400: ErrorResponse, 404: NotFoundResponse},
    )

    router.add_api_operation(
        "/{int:person_id}",
        ["GET"],
        person_handlers.get_person_by_id,
        response={200: PersonOut, 404: NotFoundResponse},
    )

    router.add_api_operation(
        "/me",
        ["GET"],
        person_handlers.get_my_info,
        response={200: PersonOut, 404: NotFoundResponse},
        url_name="my_info",
    )

    return router


def add_person_router(api: NinjaAPI, person_handlers: PersonHandlers):
    persons_handler = get_person_router(person_handlers)
    api.add_router("/people", persons_handler)
