from datetime import datetime, timedelta

import jwt
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.db import models


class AuthenticatedUserManager(BaseUserManager):
    def create_user(self, name, phone, password=None, **other_fields):
        if name is None:
            return ValueError("User must have a name")
        if phone is None:
            return ValueError("User must have a phone")
        user = self.model(name=name, phone=phone, **other_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, name, phone, password, **other_fields):
        if password is None:
            raise ValueError("Superuser must have a password")

        user = self.create_user(name, phone, password, **other_fields)
        user.is_admin = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class Person(AbstractBaseUser, PermissionsMixin):
    name = models.CharField(default="", max_length=255)
    telegram_chat_id = models.IntegerField(blank=True, null=True, unique=True)
    date_of_birth = models.DateField(default=None, blank=True, null=True)
    phone = models.CharField(default="", max_length=15, unique=True)
    favorite_users_list = models.ManyToManyField("self", symmetrical=False, blank=True)
    last_transaction_check = models.DateTimeField(default=datetime(1970, 1, 1))

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = AuthenticatedUserManager()

    USERNAME_FIELD = "phone"
    REQUIRED_FIELDS = ["name"]

    def update(self, **kwargs):
        for attr, value in kwargs.items():
            setattr(self, attr, value)
        self.save()

    def __str__(self):
        return f"{self.phone} (tg id: {self.telegram_chat_id})"

    @property
    def is_staff(self):
        return self.is_admin

    def _generate_jwt_token(self, exp_timedelta=timedelta(days=1)):
        dt = datetime.now() + exp_timedelta
        token = jwt.encode({"id": self.pk, "exp": int(dt.timestamp())}, settings.SECRET_KEY, algorithm="HS256")

        return token

    class Meta:
        verbose_name = "Person"
        verbose_name_plural = "People"
