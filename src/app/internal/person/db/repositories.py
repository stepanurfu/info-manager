from datetime import datetime
from typing import List, Optional
from uuid import UUID

from app.internal.common.exceptions import NotFoundException
from app.internal.common.utils import PersonUpdate

from ..db.models import Person
from ..domain.entities import PersonIn


class IPersonRepository:
    def get_person_by_id(self, person_id: int) -> Person:
        ...

    def get_persons(self) -> list[Person]:
        ...

    def add_or_update_person(self, person_id: int, person_data: PersonIn) -> Person:
        ...

    def get_favorite_users_list(self, person_id: int) -> list[Person]:
        ...

    def add_to_favorite_users_list(self, id_to_add: int, person_id: int) -> bool:
        ...

    def remove_from_favorite_users_list(self, id_to_remove: int, person_id: int) -> bool:
        ...

    def check_exists(self, person_id) -> bool:
        ...

    def get_person_by_phone(self, phone_number) -> Person:
        ...

    def add_or_update_only_present_fields(self, person_id: int, personUpdate: PersonUpdate = None) -> Person:
        ...

    def update_last_transaction_check(self, person: Person, new_transaction_check_datetime: datetime) -> None:
        ...


class PersonRepository(IPersonRepository):
    def check_exists(self, person_id) -> bool:
        return Person.objects.filter(pk=person_id).exists()

    def check_phone_exists(self, phone: str) -> bool:
        return Person.objects.filter(phone=phone).exists()

    def get_person_by_id(self, person_id: int) -> Optional[Person]:
        person: Optional[Person] = Person.objects.filter(pk=person_id).first()
        return person

    def get_person_id_by_telegram_id(self, telegram_chat_id: int) -> Optional[Person]:
        res = Person.objects.filter(telegram_chat_id=telegram_chat_id).values("pk").first()
        if res is None:
            return None
        return res["pk"]

    def update_password(self, person: Person, new_password: str):
        person.set_password(new_password)
        person.save(update_fields=("password",))

    def get_person_by_telegram_id(self, telegram_chat_id: int) -> Optional[Person]:
        person: Optional[Person] = Person.objects.filter(telegram_chat_id=telegram_chat_id).first()
        return person

    def get_person_by_phone(self, phone_number) -> Person:
        person: Optional[Person] = Person.objects.filter(phone=phone_number).first()
        return person

    def get_persons(self) -> List[Person]:
        return [Person.objects.all()]

    def add_or_update_person(self, person_id: int, person_data: PersonIn) -> Person:
        person, created = Person.objects.get_or_create(pk=person_id)
        if person_data is not None:
            person.update(**person_data.dict())
        return person

    def add_or_update_only_present_fields(self, telegram_chat_id: int, personUpdate: PersonUpdate = None) -> Person:
        person = Person.objects.filter(telegram_chat_id=telegram_chat_id).first()
        if person is None:
            if personUpdate is not None and personUpdate.phone is not None:
                person = Person.objects.create(telegram_chat_id=telegram_chat_id, name="", phone=personUpdate.phone)
            else:
                return None
        if personUpdate is not None:
            person.update(**personUpdate.to_dict())
        return person

    def get_favorite_users_list(self, person_id: int) -> list[Person]:
        person = self.get_person_by_id(person_id)
        if person is None:
            return None
        return person.favorite_users_list.all()

    def add_to_favorite_users_list(self, id_to_add: int, person_id: int) -> bool:
        person = self.get_person_by_id(person_id)
        person_to_add = self.get_person_by_id(id_to_add)
        if person is None or person_to_add is None:
            return False
        if person.favorite_users_list.filter(pk=id_to_add).exists():
            return False
        person.favorite_users_list.add(person_to_add)
        person.save()
        return True

    def remove_from_favorite_users_list(self, id_to_remove: int, person_id: int) -> bool:
        person = self.get_person_by_id(person_id)
        person_to_add = self.get_person_by_id(id_to_remove)
        if person is None or person_to_add is None:
            return False
        if not person.favorite_users_list.filter(pk=id_to_remove).exists():
            return False
        person.favorite_users_list.remove(person_to_add)
        person.save()
        return True

    def update_last_transaction_check(self, person: Person, new_transaction_check_datetime: datetime) -> None:
        person.last_transaction_check = new_transaction_check_datetime
        person.save(update_fields=("last_transaction_check",))
