from datetime import date
from typing import Optional

from ninja import Schema
from pydantic import Field


class PersonSchema(Schema):
    phone: str = Field("", max_length=15)
    name: str = Field("", max_length=255)
    date_of_birth: Optional[date] = None


class PersonOut(PersonSchema):
    telegram_chat_id: int = Field(None)
    id: int

    class Config:
        fields = {"id": "pk"}


class PersonIn(PersonSchema):
    ...
