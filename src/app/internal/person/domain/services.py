from typing import List
from uuid import UUID

from app.internal.common.exceptions import NotFoundException

from ..db.repositories import IPersonRepository
from .entities import PersonIn, PersonOut


class PersonService:
    def __init__(self, person_repo: IPersonRepository):
        self._person_repo = person_repo

    def get_person_by_id(self, person_id: int) -> PersonOut:
        person = self._person_repo.get_person_by_id(person_id=person_id)
        if person is None:
            raise NotFoundException("Person", person_id)
        return PersonOut.from_orm(person)

    def get_person_by_phone(self, phone_number: str) -> PersonOut:
        person = self._person_repo.get_person_by_phone(phone_number)
        if person is None:
            raise NotFoundException("Person", -1)
        return person

    def get_persons(self) -> List[PersonOut]:
        persons = self._person_repo.get_persons()
        return [PersonOut.from_orm(p) for p in persons]

    def add_person(self, person_id: int, person_data: PersonIn) -> bool:
        return self._person_repo.add_or_update_person(person_id, person_data)

    def get_favorite_users_list(self, person_id: int) -> list[PersonOut]:
        if not self._person_repo.check_exists(person_id):
            raise NotFoundException("Person", person_id)
        people = self._person_repo.get_favorite_users_list(person_id)
        return [PersonOut.from_orm(p) for p in people]

    def add_to_favorite_users_list(self, id_to_add: int, person_id: int) -> bool:
        if not self._person_repo.check_exists(person_id):
            raise NotFoundException("Person", person_id)
        if not self._person_repo.check_exists(id_to_add):
            raise NotFoundException("Person", id_to_add)
        return self._person_repo.add_to_favorite_users_list(id_to_add, person_id)

    def remove_from_favorite_users_list(self, id_to_remove: int, person_id: int) -> bool:
        if not self._person_repo.check_exists(person_id):
            raise NotFoundException("Person", person_id)
        if not self._person_repo.check_exists(id_to_remove):
            raise NotFoundException("Person", id_to_remove)
        return self._person_repo.remove_from_favorite_users_list(id_to_remove, person_id)
