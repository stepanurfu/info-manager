from ninja import NinjaAPI

from app.internal.authentication.http_jwt_auth import HTTJWTAuth

from .db.repositories import PersonRepository
from .domain.services import PersonService
from .presentation.handlers import PersonHandlers
from .presentation.routers import add_person_router


def get_api():
    api = NinjaAPI(
        title="DT.EDU.BACKEND",
        version="1.0.0",
        auth=[HTTJWTAuth()],
    )
    api = configure_person_api(api)
    return api


def configure_person_api(api: NinjaAPI):
    person_repo = PersonRepository()
    person_service = PersonService(person_repo=person_repo)
    person_handler = PersonHandlers(person_service=person_service)
    add_person_router(api, person_handler)
    return api
