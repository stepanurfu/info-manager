from ninja import NinjaAPI

from app.internal.authentication.http_jwt_auth import HTTJWTAuth
from app.internal.error_handlers.app import configure_error_handlers
from app.internal.urls import (
    configure_auth_api,
    configure_bank_account_api,
    configure_bank_card_api,
    configure_bank_transaction_api,
    configure_currency_api,
    configure_person_api,
    configure_post_card_api,
)


def get_api():
    api = NinjaAPI(
        title="DT.EDU.BACKEND",
        version="1.0.0",
        # csrf=False,
        auth=[HTTJWTAuth()],
    )
    configure_auth_api(api)
    configure_person_api(api)
    configure_currency_api(api)
    configure_bank_account_api(api)
    configure_bank_card_api(api)
    configure_bank_transaction_api(api)
    configure_error_handlers(api)
    configure_post_card_api(api)
    return api
