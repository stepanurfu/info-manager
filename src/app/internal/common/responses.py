from uuid import UUID

from ninja import Schema
from pydantic import Field


class SuccessResponse(Schema):
    success: object


class ErrorResponse(Schema):
    message: str


class NotFoundResponse(ErrorResponse):
    ...


class ConfilictResponse(ErrorResponse):
    ...


class AnauthorizedResponse(ErrorResponse):
    ...


class BadRequestResponse(ErrorResponse):
    ...
