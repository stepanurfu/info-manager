from django.db import models


class CommonModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)

    def update(self, **kwargs):
        for attr, value in kwargs.items():
            setattr(self, attr, value)
        self.save()

    class Meta:
        abstract = True
