import decimal
import re
from datetime import datetime

import pytz
from django.conf import settings
from django.utils.timezone import make_aware

float_reg = re.compile(r"^-?\d+(?:\.\d+)?$")
int_reg = re.compile(r"^-?\d+$")


def check_if_numberic(s: str):
    return float_reg.match(s) is not None


def add_timezone(date: datetime) -> datetime:
    return make_aware(date, timezone=pytz.timezone(settings.TIME_ZONE))


class PersonUpdate:
    def __init__(self, name: str = None, phone: str = None, email: str = None, date_of_birth: str = None):
        self.name = name
        self.phone = phone
        self.email = email
        self.date_of_birth = date_of_birth

    def to_dict(self):
        res = {"name": self.name, "phone": self.phone, "email": self.email, "date_of_birth": self.date_of_birth}
        return {field: val for field, val in res.items() if val is not None}


class BankTransactionInfo:
    def __init__(
        self,
        date: datetime,
        card_from: str,
        card_to: str,
        amount: decimal.Decimal,
        income: bool,
        postcard_id: int = None,
        url: str = None,
    ):
        self.date = date
        self.card_from = card_from
        self.card_to = card_to
        self.amount = amount
        self.income = income
        self.postcard_id = postcard_id
        self.url = url


class BankTransactionAccounting:
    def __init__(self, card_number: str, balance: decimal.Decimal, bank_transaction_infos: list[BankTransactionInfo]):
        self.card_number = card_number
        self.balance = balance
        self.bank_transaction_infos = bank_transaction_infos
