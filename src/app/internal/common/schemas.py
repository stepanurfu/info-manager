from ninja import Schema


class PkSchema(Schema):
    id: int

    class Config:
        fields = {"id": "pk"}


class IdSchema(Schema):
    id: int
