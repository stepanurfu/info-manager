class NotFoundException(Exception):
    def __init__(self, name, obj_id, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = name
        self.id = obj_id


class AlreadyExistException(Exception):
    def __init__(self, name, field, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = name
        self.field = field


class UnauthorizedException(Exception):
    def __init__(self, message, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.message = message


class BadRequestException(Exception):
    def __init__(self, message, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.message = message
