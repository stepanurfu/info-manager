from ninja import NinjaAPI


def object_not_found_exception_handler(request, exc, api: NinjaAPI):
    return api.create_response(
        request,
        {"message": f"{exc.name} with id {exc.id} not found"},
        status=404,
    )


def integrity_exception_handler(request, exc, api: NinjaAPI):
    return api.create_response(
        request,
        {"message": f"{exc.name} with field {exc.field} already exist"},
        status=400,
    )


def unauthorized_exception_handler(request, exc, api: NinjaAPI):
    return api.create_response(
        request,
        {"message": f"{exc.message}"},
        status=404,
    )


def bad_request_exception_handler(request, exc, api: NinjaAPI):
    return api.create_response(
        request,
        {"message": f"{exc.message}"},
        status=404,
    )
