from functools import partial

from ninja import NinjaAPI

from app.internal.common.exceptions import (
    AlreadyExistException,
    BadRequestException,
    NotFoundException,
    UnauthorizedException,
)

from .handlers import (
    bad_request_exception_handler,
    integrity_exception_handler,
    object_not_found_exception_handler,
    unauthorized_exception_handler,
)


def configure_error_handlers(api: NinjaAPI):
    api.add_exception_handler(NotFoundException, partial(object_not_found_exception_handler, api=api))
    api.add_exception_handler(AlreadyExistException, partial(integrity_exception_handler, api=api))
    api.add_exception_handler(UnauthorizedException, partial(unauthorized_exception_handler, api=api))
    api.add_exception_handler(BadRequestException, partial(bad_request_exception_handler, api=api))
    return api
