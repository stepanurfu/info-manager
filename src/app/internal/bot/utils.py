import datetime
import decimal
import re

from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.utils.timezone import localtime

from app.internal.common.utils import BankTransactionAccounting, BankTransactionInfo, add_timezone, check_if_numberic
from app.internal.person.db.models import Person
from app.internal.post_card.db.repositories import PostCardRepository


def build_fill_prompt_message(field_name: str, prev_value=None, is_mandatory=False):
    if prev_value is None or prev_value == "":
        field_marker = " [mandatory]" if is_mandatory else ""
        return f"Tell me your {field_name}{field_marker}:"
    msg = f"Your {field_name} is set to:\n" f"{prev_value}\n\n" "Enter new value (or /skip):\n"
    return msg


def build_fill_success_message(field_name: str, prev_value, new_value) -> str:
    if prev_value is None:
        return f"{field_name} is saved:\n{new_value}"
    msg = f"{field_name} changed to:\n" f"{new_value}"
    return msg


def normailze_date(date_str: str):
    date_formats = ("%Y-%m-%d", "%d.%m.%Y", "%d/%m/%Y")
    for date_format in date_formats:
        try:
            return datetime.datetime.strptime(date_str, date_format).strftime("%Y-%m-%d")
        except ValueError:
            pass
    return None


def parse_date(date_str: str) -> datetime.date:
    normalized = normailze_date(date_str)
    if normalized:
        dt = datetime.datetime.strptime(normalized, "%Y-%m-%d")
        return add_timezone(dt).date()
    return None


phone_pattern = r"^(\+?\d?)?[\s\-]?\(?([0-9]{3})\)?[\s\-\.]?([0-9]{3})" r"[\s\-\.]?([0-9]{2})[\s\-\.]?([0-9]{2})$"
phone_regex = re.compile(phone_pattern)


def normalize_phone(phone_str: str) -> str:
    match = phone_regex.match(phone_str)
    if match is None:
        return None
    return "".join(match.groups())


def check_email(email_str: str) -> bool:
    try:
        validate_email(email_str)
    except ValidationError:
        return False
    else:
        return True


BANK_ACCOUNT_NUMBER_LENGTH = 20


def check_if_bank_account_number(number: str) -> bool:
    return len(number) == BANK_ACCOUNT_NUMBER_LENGTH


def format_person_cards(person_name: str, accont_cards: dict[str, str]) -> str:
    parts_title = [f"Accounts and Cards of {person_name}:"]
    parts = []
    for bank_account_number in accont_cards.keys():
        parts.append(f"\nBank Account: {bank_account_number}")
        for i, card_number in enumerate(accont_cards[bank_account_number]):
            parts.append(f" - Card {i + 1}: {card_number}")
    if not len(parts):
        parts = ["Empty"]
    return "\n".join(parts_title + parts)


space_regex = re.compile(r"\s+")


class TransferInfo:
    def __init__(self, card_from, card_to, amount: decimal.Decimal):
        self.card_from = card_from
        self.card_to = card_to
        self.amount = amount

    @classmethod
    def parse_transfer_arguments(cls, args_str: str) -> "TransferInfo":
        args_str = space_regex.sub(" ", args_str).strip()
        args_parts = args_str.split(" ")
        if len(args_parts) != 3:
            return None
        if not check_if_numberic(args_parts[-1]):
            return None
        return cls(args_parts[0], args_parts[1], decimal.Decimal(args_parts[2]))


class StatementInfo:
    def __init__(self, number, start_date, end_date):
        self.number = number
        self.start_date = start_date
        self.end_date = end_date

    @classmethod
    def parse_statement_arguments(cls, args_str: str) -> "StatementInfo":
        args_str = space_regex.sub(" ", args_str).strip()
        args_parts = args_str.split(" ")
        start_date = datetime.date(1970, 1, 1)
        end_date = datetime.date.today()
        if len(args_parts) == 0 or len(args_parts) == 1 and args_parts[0] == "":
            return None
        number = args_parts[0]
        if len(args_parts) > 1:
            start_date = parse_date(args_parts[1])
        if len(args_parts) > 2:
            end_date = parse_date(args_parts[2])
        return cls(number, start_date, end_date)


def format_bank_transaction_info(transaction_info: BankTransactionInfo, show_url=True) -> str:
    sign = "+" if transaction_info.income else "-"
    res = "{}: [{} -> {}] {}{}".format(
        localtime(transaction_info.date).strftime("%Y-%m-%d %H:%M:%S"),
        transaction_info.card_from,
        transaction_info.card_to,
        sign,
        transaction_info.amount,
    )
    if show_url and transaction_info.url is not None:
        res += f"\n{transaction_info.url}\n"
    return res


def format_bank_transaction_info_for_card(bank_transaction_infos: BankTransactionAccounting) -> str:
    res = [
        format_bank_transaction_info(bank_transaction_info)
        for bank_transaction_info in bank_transaction_infos.bank_transaction_infos
    ]
    statements = [f"Statements of {bank_transaction_infos.card_number}:"] + res
    statements.append(f"> Balance: {bank_transaction_infos.balance}")
    return "\n".join(statements)


def format_bank_transaction_info_for_account(
    bank_account_number: str, bank_transaction_infos_per_card: list[BankTransactionAccounting]
) -> str:
    statement_parts = [f"Statements of {bank_account_number}:"]
    for i, bank_transaction_accounting in enumerate(bank_transaction_infos_per_card):
        statement_parts.append(f"\n    Card {i + 1}: {bank_transaction_accounting.card_number}")
        for bank_transaction_info in bank_transaction_accounting.bank_transaction_infos:
            statement_parts.append(format_bank_transaction_info(bank_transaction_info))
        statement_parts.append(f"> Balance: {bank_transaction_accounting.balance}")
    return "\n".join(statement_parts)


def format_transfer_relative_names(names: set[str]) -> str:
    return "\n".join([f" - {n}" for n in names if n is not None])


def generate_filename(chat_id, init_file_name):
    return "{}_{}_{}".format(chat_id, datetime.datetime.now().timestamp(), init_file_name)
