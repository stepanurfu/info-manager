import logging

from django.conf import settings
from django.urls import reverse_lazy
from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, ConversationHandler, Filters, MessageHandler

from app.internal.common.utils import PersonUpdate
from app.internal.person.db.repositories import PersonRepository

from ..base_handlers import BotState, cancel, create_conversation_handler, if_have_message
from ..response_messages import PersonInfoResponseFormats, build_help_message, build_person_info_message
from ..utils import build_fill_prompt_message, normailze_date, normalize_phone

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


_person_repository = PersonRepository()


@if_have_message
def start(update: Update, context: CallbackContext):
    update.message.reply_text(PersonInfoResponseFormats.START)
    return


@if_have_message
def fill_interactive(update: Update, context: CallbackContext) -> BotState:
    person = _person_repository.get_person_by_telegram_id(update.message.chat_id)
    prev_phone_value = None
    if person:
        prev_phone_value = person.phone
    update.message.reply_text(PersonInfoResponseFormats.FILL_INTERACTIVE)
    update.message.reply_text(build_fill_prompt_message("Phone", prev_phone_value))
    return BotState.PHONE


@if_have_message
def phone(update: Update, context: CallbackContext) -> BotState:
    phone_number = normalize_phone(update.message.text)
    if phone_number is None:
        update.message.reply_text(PersonInfoResponseFormats.PHONE_INVALID_FORMAT_TRY_AGAIN)
        return BotState.PHONE
    if _person_repository.check_phone_exists(phone_number):
        update.message.reply_text(PersonInfoResponseFormats.PHONE_ALREADY_EXISTS_TRY_AGAIN)
        return BotState.PHONE
    person = _person_repository.add_or_update_only_present_fields(
        telegram_chat_id=update.message.chat_id, personUpdate=PersonUpdate(phone=phone_number)
    )
    update.message.reply_text(build_fill_prompt_message("Name", person.name, is_mandatory=True))
    return BotState.NAME


@if_have_message
def skip_phone(update: Update, context: CallbackContext) -> BotState:
    person = _person_repository.get_person_by_telegram_id(update.message.chat_id)
    if not person or not person.phone:
        update.message.reply_text(PersonInfoResponseFormats.CANNOT_SKIP_MANDATORY_FIELD)
        return BotState.PHONE
    update.message.reply_text(build_fill_prompt_message("Name", person.name))
    return BotState.NAME


@if_have_message
def name(update: Update, context: CallbackContext) -> BotState:
    new_name = update.message.text
    if len(new_name) >= 254:
        update.message.reply_text(PersonInfoResponseFormats.NAME_IS_TOO_LONG_TRY_AGAIN)
        return BotState.NAME
    person = _person_repository.add_or_update_only_present_fields(
        telegram_chat_id=update.message.chat_id, personUpdate=PersonUpdate(name=new_name)
    )
    update.message.reply_text(build_fill_prompt_message("Date of birth", person.date_of_birth))
    return BotState.DATE_OF_BIRTH


@if_have_message
def skip_name(update: Update, context: CallbackContext) -> BotState:
    person = _person_repository.get_person_by_telegram_id(update.message.chat_id)
    update.message.reply_text(build_fill_prompt_message("Date of birth", person.date_of_birth))
    return BotState.DATE_OF_BIRTH


@if_have_message
def date_of_birth(update: Update, context: CallbackContext) -> BotState:
    person_date_of_birth = normailze_date(update.message.text)
    if person_date_of_birth is None:
        update.message.reply_text(PersonInfoResponseFormats.DATE_OF_BIRTH_INVALID_FORMAT_TRY_AGAIN)
        return BotState.DATE_OF_BIRTH
    _person_repository.add_or_update_only_present_fields(
        telegram_chat_id=update.message.chat_id, personUpdate=PersonUpdate(date_of_birth=person_date_of_birth)
    )
    update.message.reply_text(PersonInfoResponseFormats.INFO_FILL_COMPLETED)
    return ConversationHandler.END


@if_have_message
def skip_date_of_birth(update: Update, context: CallbackContext) -> BotState:
    update.message.reply_text(PersonInfoResponseFormats.INFO_FILL_COMPLETED)
    return ConversationHandler.END


@if_have_message
def set_phone_cmd(update: Update, context: CallbackContext) -> BotState:
    update.message.reply_text(build_fill_prompt_message("Phone"))
    return BotState.PHONE


@if_have_message
def set_phone_msg(update: Update, context: CallbackContext) -> BotState:
    phone_number = normalize_phone(update.message.text)
    if phone_number is None:
        update.message.reply_text(PersonInfoResponseFormats.PHONE_INVALID_FORMAT)
        return ConversationHandler.END
    if _person_repository.check_phone_exists(phone_number):
        update.message.reply_text(PersonInfoResponseFormats.PHONE_ALREADY_EXISTS)
        return ConversationHandler.END
    _person_repository.add_or_update_only_present_fields(
        telegram_chat_id=update.message.chat_id, personUpdate=PersonUpdate(phone=phone_number)
    )
    return ConversationHandler.END


@if_have_message
def set_name_cmd(update: Update, context: CallbackContext) -> BotState:
    person = _person_repository.get_person_by_telegram_id(update.message.chat_id)
    if not person or not person.phone:
        update.message.reply_text(PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED)
        return ConversationHandler.END
    update.message.reply_text(build_fill_prompt_message("Name"))
    return BotState.NAME


@if_have_message
def set_name_msg(update: Update, context: CallbackContext) -> BotState:
    new_name = update.message.text
    if len(new_name) >= 254:
        update.message.reply_text(PersonInfoResponseFormats.NAME_IS_TOO_LONG)
        return ConversationHandler.END
    _person_repository.add_or_update_only_present_fields(
        telegram_chat_id=update.message.chat_id, personUpdate=PersonUpdate(name=update.message.text)
    )
    return ConversationHandler.END


@if_have_message
def set_date_of_birth_cmd(update: Update, context: CallbackContext) -> BotState:
    person = _person_repository.get_person_by_telegram_id(update.message.chat_id)
    if not person or not person.phone:
        update.message.reply_text(PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED)
        return ConversationHandler.END
    update.message.reply_text(build_fill_prompt_message("Date of birth"))
    return BotState.DATE_OF_BIRTH


@if_have_message
def set_date_of_birth_msg(update: Update, context: CallbackContext) -> BotState:
    person_date_of_birth = normailze_date(update.message.text)
    if person_date_of_birth is None:
        update.message.reply_text(PersonInfoResponseFormats.DATE_OF_BIRTH_INVALID_FORMAT_TRY_AGAIN)
        return BotState.DATE_OF_BIRTH
    _person_repository.add_or_update_only_present_fields(
        telegram_chat_id=update.message.chat_id, personUpdate=PersonUpdate(date_of_birth=person_date_of_birth)
    )
    return ConversationHandler.END


@if_have_message
def set_password_cmd(update: Update, context: CallbackContext) -> BotState:
    person = _person_repository.get_person_by_telegram_id(update.message.chat_id)
    if not person or not person.phone:
        update.message.reply_text(PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED)
        return ConversationHandler.END
    update.message.reply_text(PersonInfoResponseFormats.ENTER_NEW_PASSWORD)
    return BotState.PASSWORD


@if_have_message
def set_password_msg(update: Update, context: CallbackContext) -> BotState:
    new_password = update.message.text
    person = _person_repository.get_person_by_telegram_id(update.message.chat_id)
    _person_repository.update_password(person, new_password)
    return ConversationHandler.END


@if_have_message
def me(update: Update, context: CallbackContext) -> BotState:
    person = _person_repository.get_person_by_telegram_id(update.message.chat_id)
    if not person or not person.phone:
        update.message.reply_text(PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED)
        return
    update.message.reply_text(build_person_info_message(person))


@if_have_message
def help(update: Update, context: CallbackContext) -> BotState:
    link = f"http://{settings.HOST_NAME}" + f'{reverse_lazy("api-1.0.0:my_info")}'
    update.message.reply_text(build_help_message(link))


def error(bot, update, error):
    if error.message == "Message is not modified":
        return
    logger.warning('Update "%s" caused error "%s"' % (update, error))


start_handler = CommandHandler("start", start)
help_handler = CommandHandler("help", help)
me_handler = CommandHandler("me", me)


full_conversation_handler = ConversationHandler(
    entry_points=[CommandHandler("fill_interactive", fill_interactive)],
    states={
        BotState.PHONE: [MessageHandler(Filters.text & ~(Filters.command), phone), CommandHandler("skip", skip_phone)],
        BotState.NAME: [MessageHandler(Filters.text & ~Filters.command, name), CommandHandler("skip", skip_name)],
        BotState.DATE_OF_BIRTH: [
            MessageHandler(Filters.text & ~Filters.command, date_of_birth),
            CommandHandler("skip", skip_date_of_birth),
        ],
    },
    fallbacks=[CommandHandler("cancel", cancel)],
)


phone_conversation_handler = create_conversation_handler("set_phone", set_phone_cmd, {BotState.PHONE: set_phone_msg})


name_conversation_handler = create_conversation_handler("set_name", set_name_cmd, {BotState.NAME: set_name_msg})


date_of_birth_conversation_handler = create_conversation_handler(
    "set_date_of_birth", set_date_of_birth_cmd, {BotState.DATE_OF_BIRTH: set_date_of_birth_msg}
)

password_conversation_handler = create_conversation_handler(
    "set_password", set_password_cmd, {BotState.PASSWORD: set_password_msg}
)
