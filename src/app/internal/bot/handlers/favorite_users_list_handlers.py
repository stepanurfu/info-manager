from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, ConversationHandler, Filters, MessageHandler

from app.internal.common.utils import check_if_numberic
from app.internal.person.db.repositories import PersonRepository
from app.internal.person.presentation.telegram_format import format_favorite_users_list

from ..base_handlers import BotState, create_conversation_handler, if_have_message
from ..response_messages import PersonInfoResponseFormats

_person_repository = PersonRepository()


@if_have_message
def show_favorite_users_list_command(update: Update, context: CallbackContext) -> BotState:
    person = _person_repository.get_person_by_telegram_id(update.message.chat_id)
    if not person or not person.phone:
        update.message.reply_text(PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED)
        return ConversationHandler.END
    list_formatted = format_favorite_users_list(person)
    update.message.reply_text(list_formatted)


show_favorite_users_list_handler = CommandHandler("show_favorite_users_list", show_favorite_users_list_command)


@if_have_message
def add_to_favorite_users_cmd(update: Update, context: CallbackContext) -> BotState:
    person = _person_repository.get_person_by_telegram_id(update.message.chat_id)
    if not person or not person.phone:
        update.message.reply_text(PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED)
        return ConversationHandler.END
    update.message.reply_text(PersonInfoResponseFormats.ENTER_USER_ID)
    return BotState.FAVORITE_LIST


@if_have_message
def add_to_favorite_users_msg(update: Update, context: CallbackContext) -> BotState:
    other_person_telegram_id_str = update.message.text
    if not check_if_numberic(other_person_telegram_id_str):
        update.message.reply_text(PersonInfoResponseFormats.NO_USER_WITH_ID)
        return ConversationHandler.END

    other_person_telegram_id = int(float(other_person_telegram_id_str))
    other_person_id = _person_repository.get_person_id_by_telegram_id(other_person_telegram_id)
    if other_person_id is None:
        update.message.reply_text(PersonInfoResponseFormats.NO_USER_WITH_ID)
        return ConversationHandler.END
    current_person_id = _person_repository.get_person_id_by_telegram_id(update.message.chat_id)
    success = _person_repository.add_to_favorite_users_list(other_person_id, current_person_id)
    if success:
        update.message.reply_text(PersonInfoResponseFormats.FAVORITE_USERS_LIST_ADD_SUCCESS)
    else:
        update.message.reply_text(PersonInfoResponseFormats.FAVORITE_USERS_LIST_ADD_FAIL)
    return ConversationHandler.END


@if_have_message
def remove_from_favorite_users_list_cmd(update: Update, context: CallbackContext) -> BotState:
    person = _person_repository.get_person_by_telegram_id(update.message.chat_id)
    if not person or not person.phone:
        update.message.reply_text(PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED)
        return ConversationHandler.END
    update.message.reply_text(PersonInfoResponseFormats.ENTER_USER_ID)
    return BotState.FAVORITE_LIST


@if_have_message
def remove_from_favorite_users_list_msg(update: Update, context: CallbackContext) -> BotState:
    other_person_telegram_id_str = update.message.text
    if not check_if_numberic(other_person_telegram_id_str):
        update.message.reply_text(PersonInfoResponseFormats.NO_USER_WITH_ID)
        return ConversationHandler.END

    other_person_telegram_id = int(float(other_person_telegram_id_str))
    other_person_id = _person_repository.get_person_id_by_telegram_id(other_person_telegram_id)
    if other_person_id is None:
        update.message.reply_text(PersonInfoResponseFormats.NO_USER_WITH_ID)
        return ConversationHandler.END

    current_person_id = _person_repository.get_person_id_by_telegram_id(update.message.chat_id)
    success = _person_repository.remove_from_favorite_users_list(other_person_id, current_person_id)
    if success:
        update.message.reply_text(PersonInfoResponseFormats.FAVORITE_USERS_LIST_REMOVE_SUCCESS)
    else:
        update.message.reply_text(PersonInfoResponseFormats.FAVORITE_USERS_LIST_REMOVE_FAIL)
    return ConversationHandler.END


add_to_favorite_users_conversation_handler = create_conversation_handler(
    "add_to_favorite_users_list", add_to_favorite_users_cmd, {BotState.FAVORITE_LIST: add_to_favorite_users_msg}
)


remove_from_favorite_users_list_conversation_handler = create_conversation_handler(
    "remove_from_favorite_users_list",
    remove_from_favorite_users_list_cmd,
    {BotState.FAVORITE_LIST: remove_from_favorite_users_list_msg},
)
