import datetime

from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, ConversationHandler

from app.internal.bank_account.db.repositories import BankAccountRepository
from app.internal.bank_card.db.repositories import BankCardRepository
from app.internal.bank_transaction.db.repositories import BankTransactionRepository
from app.internal.bank_transaction.domain.services import BankTransactionService
from app.internal.common.utils import add_timezone, check_if_numberic
from app.internal.person.db.repositories import PersonRepository
from app.internal.post_card.db.models import PostCard
from app.internal.post_card.db.repositories import PostCardRepository
from app.internal.post_card.domain.entities import PostCardIn

from ..base_handlers import BotState, create_conversation_handler, if_have_message
from ..response_messages import PersonInfoResponseFormats
from ..utils import (
    StatementInfo,
    TransferInfo,
    format_bank_transaction_info,
    format_bank_transaction_info_for_account,
    format_bank_transaction_info_for_card,
    format_person_cards,
    format_transfer_relative_names,
    generate_filename,
)

_person_repo = PersonRepository()
_bank_card_repo = BankCardRepository()
_bank_account_repo = BankAccountRepository()
_bank_transaction_repo = BankTransactionRepository()
_bank_transaction_service = BankTransactionService(_bank_transaction_repo, _bank_account_repo, _bank_card_repo)
_postcard_repository = PostCardRepository()


@if_have_message
def show_balance_cmd(update: Update, context: CallbackContext) -> BotState:
    person = _person_repo.get_person_by_telegram_id(update.message.chat_id)
    if not person or not person.phone:
        update.message.reply_text(PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED)
        return ConversationHandler.END
    update.message.reply_text(PersonInfoResponseFormats.ENTER_BANK_ACCOUNT_CARD_NUMBER)
    return BotState.BANK_ACCOUNT


@if_have_message
def show_card_balance_cmd(update: Update, context: CallbackContext) -> BotState:
    person = _person_repo.get_person_by_telegram_id(update.message.chat_id)
    if not person or not person.phone:
        update.message.reply_text(PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED)
        return ConversationHandler.END
    update.message.reply_text(PersonInfoResponseFormats.ENTER_BANK_CARD_NUMBER)
    return BotState.BANK_CARD


@if_have_message
def show_card_balance_msg(update: Update, context: CallbackContext) -> BotState:
    person = _person_repo.get_person_by_telegram_id(update.message.chat_id)
    number = update.message.text
    bank_card = _bank_card_repo.get_bank_card_by_number(person, number)
    if not bank_card:
        update.message.reply_text(PersonInfoResponseFormats.BANK_ACCOUNT_CARD_NUMBER_NOT_FOUND)
    else:
        balance = bank_card.balance
        update.message.reply_text(PersonInfoResponseFormats.BANK_CARD_BALANCE.format(balance))
    return ConversationHandler.END


@if_have_message
def show_account_balance_cmd(update: Update, context: CallbackContext) -> BotState:
    person = _person_repo.get_person_by_telegram_id(update.message.chat_id)
    if not person or not person.phone:
        update.message.reply_text(PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED)
        return ConversationHandler.END
    update.message.reply_text(PersonInfoResponseFormats.ENTER_BANK_ACCOUNT_NUMBER)
    return BotState.BANK_ACCOUNT


@if_have_message
def show_account_balance_msg(update: Update, context: CallbackContext) -> BotState:
    person = _person_repo.get_person_by_telegram_id(update.message.chat_id)
    number = update.message.text
    bank_account = _bank_account_repo.get_bank_account_by_number(person, number)
    if not bank_account:
        update.message.reply_text(PersonInfoResponseFormats.BANK_ACCOUNT_CARD_NUMBER_NOT_FOUND)
    else:
        balance = _bank_account_repo.get_bank_account_balance(bank_account)
        update.message.reply_text(PersonInfoResponseFormats.BANK_ACCOUNT_CARD_NUMBER_BALANCE.format(balance))
    return ConversationHandler.END


@if_have_message
def transfer_by_card_cmd(update: Update, context: CallbackContext) -> BotState:
    person = _person_repo.get_person_by_telegram_id(update.message.chat_id)
    if not person or not person.phone:
        update.message.reply_text(PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED)
        return ConversationHandler.END
    update.message.reply_text(PersonInfoResponseFormats.ENTER_TRANSFER_INFO_BY_BANK_CARD)
    return BotState.TRANSFER


@if_have_message
def transfer_by_card_msg(update: Update, context: CallbackContext) -> BotState:
    person = _person_repo.get_person_by_telegram_id(update.message.chat_id)

    arguments = update.message.text
    parsed_args = TransferInfo.parse_transfer_arguments(arguments)
    if parsed_args is None:
        update.message.reply_text(PersonInfoResponseFormats.TRANSFER_BY_BANK_CARD_FAIL)
        return ConversationHandler.END

    success = _bank_transaction_repo.transfer_by_card(
        person, parsed_args.card_from, parsed_args.card_to, parsed_args.amount
    )
    if not success:
        update.message.reply_text(PersonInfoResponseFormats.TRANSFER_BY_BANK_CARD_FAIL)
    else:
        update.message.reply_text(PersonInfoResponseFormats.TRANSFER_BY_BANK_CARD_SUCCESS)
    return ConversationHandler.END


@if_have_message
def transfer_by_card_with_postcard_cmd(update: Update, context: CallbackContext) -> BotState:
    person = _person_repo.get_person_by_telegram_id(update.message.chat_id)
    if not person or not person.phone:
        update.message.reply_text(PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED)
        return ConversationHandler.END
    update.message.reply_text(PersonInfoResponseFormats.ENTER_TRANSFER_INFO_BY_BANK_CARD_WITH_POSTCARD)
    return BotState.TRANSFER


@if_have_message
def transfer_by_card_with_postcard_msg(update: Update, context: CallbackContext) -> BotState:
    chat_id = update.message.chat_id
    person = _person_repo.get_person_by_telegram_id(chat_id)

    arguments = update.message.caption

    if not arguments:
        update.message.reply_text(PersonInfoResponseFormats.NO_CAPTION)
        return ConversationHandler.END

    parsed_args = TransferInfo.parse_transfer_arguments(arguments)
    if parsed_args is None:
        update.message.reply_text(PersonInfoResponseFormats.TRANSFER_BY_BANK_CARD_FAIL)
        return ConversationHandler.END

    bank_transaction = _bank_transaction_repo.transfer_by_card(
        person, parsed_args.card_from, parsed_args.card_to, parsed_args.amount
    )
    if bank_transaction is None:
        update.message.reply_text(PersonInfoResponseFormats.TRANSFER_BY_BANK_CARD_FAIL)
        return ConversationHandler.END
    if update.message.document:
        file_name = generate_filename(chat_id, update.message.document.file_name)
        file = context.bot.get_file(update.message.document.file_id)
        content = file.download_as_bytearray()

        postcard_in = PostCardIn(content=content)
        postcard = _postcard_repository.add_post_card(postcard_in, file_name=file_name)
        _bank_transaction_repo.attach_post_card(bank_transaction=bank_transaction, postcard=postcard)

    update.message.reply_text(PersonInfoResponseFormats.TRANSFER_BY_BANK_CARD_SUCCESS)
    return ConversationHandler.END


@if_have_message
def show_person_cards_cmd(update: Update, context: CallbackContext) -> BotState:
    person = _person_repo.get_person_by_telegram_id(update.message.chat_id)
    if not person or not person.phone:
        update.message.reply_text(PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED)
        return ConversationHandler.END
    update.message.reply_text(PersonInfoResponseFormats.ENTER_USER_ID)
    return BotState.BANK_CARD


@if_have_message
def show_person_cards_msg(update: Update, context: CallbackContext) -> BotState:
    other_person_telegram_id_str = update.message.text
    if not check_if_numberic(other_person_telegram_id_str):
        update.message.reply_text(PersonInfoResponseFormats.NO_USER_WITH_ID)
        return ConversationHandler.END
    other_person_telegram_id = int(float(other_person_telegram_id_str))
    other_person = _person_repo.get_person_by_telegram_id(other_person_telegram_id)
    if other_person is None:
        update.message.reply_text(PersonInfoResponseFormats.NO_USER_WITH_ID)
        return ConversationHandler.END
    account_card_numbers = _bank_card_repo.get_person_cards(other_person)
    cards_formatted = format_person_cards(other_person.name, account_card_numbers)
    update.message.reply_text(cards_formatted)
    return ConversationHandler.END


@if_have_message
def show_statements_by_card_cmd(update: Update, context: CallbackContext) -> BotState:
    person = _person_repo.get_person_by_telegram_id(update.message.chat_id)
    if not person or not person.phone:
        update.message.reply_text(PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED)
        return ConversationHandler.END
    update.message.reply_text(PersonInfoResponseFormats.ENTER_BANK_CARD_NUMBER_WITH_DATE_RANGE)
    return BotState.STATEMENTS


@if_have_message
def show_statements_by_card_msg(update: Update, context: CallbackContext) -> BotState:
    person = _person_repo.get_person_by_telegram_id(update.message.chat_id)

    parsed_args = StatementInfo.parse_statement_arguments(update.message.text)
    bank_transaction_infos = _bank_transaction_service.get_transaction_info_by_card_number(
        person, parsed_args.number, parsed_args.start_date, parsed_args.end_date
    )
    if bank_transaction_infos is None:
        update.message.reply_text(PersonInfoResponseFormats.BANK_ACCOUNT_CARD_NUMBER_NOT_FOUND)
        return ConversationHandler.END
    statements = format_bank_transaction_info_for_card(bank_transaction_infos)
    update.message.reply_text(statements)
    return ConversationHandler.END


@if_have_message
def show_statements_by_account_cmd(update: Update, context: CallbackContext) -> BotState:
    person = _person_repo.get_person_by_telegram_id(update.message.chat_id)
    if not person or not person.phone:
        update.message.reply_text(PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED)
        return ConversationHandler.END
    update.message.reply_text(PersonInfoResponseFormats.ENTER_BANK_ACCOUNT_NUMBER_WITH_DATE_RANGE)
    return BotState.STATEMENTS


@if_have_message
def show_statements_by_account_msg(update: Update, context: CallbackContext) -> BotState:
    person = _person_repo.get_person_by_telegram_id(update.message.chat_id)

    parsed_args = StatementInfo.parse_statement_arguments(update.message.text)
    bank_transaction_infos_per_card = _bank_transaction_service.get_transaction_info_by_account_number(
        person, parsed_args.number, parsed_args.start_date, parsed_args.end_date
    )
    if bank_transaction_infos_per_card is None:
        update.message.reply_text(PersonInfoResponseFormats.BANK_ACCOUNT_CARD_NUMBER_NOT_FOUND)
        return ConversationHandler.END
    statements = format_bank_transaction_info_for_account(parsed_args.number, bank_transaction_infos_per_card)
    update.message.reply_text(statements)
    return ConversationHandler.END


bank_card_balance_conversation_handler = create_conversation_handler(
    "show_card_balance", show_card_balance_cmd, {BotState.BANK_CARD: show_card_balance_msg}
)


bank_account_balance_conversation_handler = create_conversation_handler(
    "show_account_balance", show_account_balance_cmd, {BotState.BANK_ACCOUNT: show_account_balance_msg}
)


transfer_by_card_conversation_handler = create_conversation_handler(
    "transfer_by_card", transfer_by_card_cmd, {BotState.TRANSFER: transfer_by_card_msg}
)

transfer_by_card_with_postcard_conversation_handler = create_conversation_handler(
    "transfer_by_card_with_postcard",
    transfer_by_card_with_postcard_cmd,
    {BotState.TRANSFER: transfer_by_card_with_postcard_msg},
    allow_document=True,
)


show_person_cards_conversation_handler = create_conversation_handler(
    "show_person_cards", show_person_cards_cmd, {BotState.BANK_CARD: show_person_cards_msg}
)

show_statements_by_card_conversation_handler = create_conversation_handler(
    "show_statements_by_card", show_statements_by_card_cmd, {BotState.STATEMENTS: show_statements_by_card_msg}
)

show_statements_by_account_conversation_handler = create_conversation_handler(
    "show_statements_by_account", show_statements_by_account_cmd, {BotState.STATEMENTS: show_statements_by_account_msg}
)


@if_have_message
def show_transfer_relative_names(update: Update, context: CallbackContext) -> BotState:
    person = _person_repo.get_person_by_telegram_id(update.message.chat_id)
    if not person or not person.phone:
        update.message.reply_text(PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED)
        return ConversationHandler.END

    names = _bank_transaction_repo.get_all_transfer_relative_names(person)
    names_formatted = format_transfer_relative_names(names)
    update.message.reply_text("Transfer relative names:\n" + names_formatted)


@if_have_message
def show_new_transactions(update: Update, context: CallbackContext) -> BotState:
    chat_id = update.message.chat_id
    person = _person_repo.get_person_by_telegram_id(chat_id)
    if not person or not person.phone:
        update.message.reply_text(PersonInfoResponseFormats.PHONE_NUMBER_MUST_BE_SPECIFIED)
        return ConversationHandler.END

    new_transaction_check_datetime = add_timezone(datetime.datetime.now())
    accountings = _bank_transaction_repo.get_new_transactions(person)
    _person_repo.update_last_transaction_check(person, new_transaction_check_datetime)
    count = 0
    for accounting in accountings:
        for transaction_info in accounting.bank_transaction_infos:
            count += 1
            formatted = format_bank_transaction_info(transaction_info, False)
            update.message.reply_text(formatted)
            if transaction_info.postcard_id is not None:
                postcard_content = PostCard.objects.get(pk=transaction_info.postcard_id).content
                update.message.reply_document(postcard_content.file.open("rb"))
    if not count:
        update.message.reply_text("No new transactions")


show_transfer_relative_names_handler = CommandHandler("show_transfer_relative_names", show_transfer_relative_names)


show_new_transactions_handler = CommandHandler("show_new_transactions", show_new_transactions)
