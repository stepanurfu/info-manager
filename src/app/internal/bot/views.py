import json

from django.http import JsonResponse
from django.views import View

from .bot import telegram_bot_webhook


class TelegramBotWebhookView(View):
    def post(self, request, *args, **kwargs):
        telegram_bot_webhook.process(json.loads(request.body))
        return JsonResponse({"ok": "processed"})

    def get(self, request, *args, **kwargs):
        return JsonResponse({"ok": "not processed"})
