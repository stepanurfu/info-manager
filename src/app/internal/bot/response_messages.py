from enum import Enum

from app.internal.person.db.models import Person


class PersonInfoResponseFormats(str, Enum):
    START = (
        "Hi! I am bot for storing your personal data.\n\n"
        "To insert data interactively use /fill_interactive\n\n"
        "To see all commands use /help\n\n"
    )
    FILL_INTERACTIVE = (
        "Hi! I am bot for storing your personal data.\n\n"
        "To skip step use /skip (note: mandatory field cannot be skipped)\n"
        "To stop interactive mode use /cancel\n\n"
    )
    CANNOT_SKIP_MANDATORY_FIELD = "Field is mandatory, cannot be skipped"
    CANCEL = "Canceled"

    PHONE_INVALID_FORMAT_TRY_AGAIN = "Failed to parse phone number.\n\nTry input just numbers:"
    NAME_IS_TOO_LONG = "Name is too long."
    NAME_IS_TOO_LONG_TRY_AGAIN = "Name is too long.\n\nUse something shorter:"
    PHONE_INVALID_FORMAT = "Failed to parse phone number."
    EMAIL_INVALID_FORMAT_TRY_AGAIN = "Enter a valid email address:"
    DATE_OF_BIRTH_INVALID_FORMAT_TRY_AGAIN = "Date of birth has incorrect format.\n\n" "Try using YYYY-mm-dd format"

    INFO_FILL_COMPLETED = "Thank you for the answers, data is saved"
    PHONE_NUMBER_MUST_BE_SPECIFIED = "Specify Phone number first using /set_phone"

    ENTER_BANK_ACCOUNT_CARD_NUMBER = "Enter Bank Account Number/Card number:"
    ENTER_BANK_CARD_NUMBER = "Enter Bank Account Card number:"
    ENTER_BANK_ACCOUNT_NUMBER = "Enter Bank Account number:"

    BANK_ACCOUNT_CARD_NUMBER_NOT_FOUND = "Number not found"

    BANK_ACCOUNT_CARD_NUMBER_BALANCE = "Bank Account balance is {0}"
    BANK_CARD_BALANCE = "Bank Card balance is {0}"

    TRANSFER_BY_BANK_CARD_SUCCESS = "Translation successfully completed"
    ENTER_TRANSFER_INFO_BY_BANK_CARD_WITH_POSTCARD = (
        "Attach image\n" "Add caption <your card number> <dest card number> <amount>"
    )
    NO_CAPTION = "Transfer failed, no caption specified"

    TRANSFER_BY_BANK_CARD_FAIL = "Transfer failed"
    ENTER_YOUR_BANK_CARD_NUMBER = "Enter Your bank card number:"

    ENTER_TRANSFER_INFO_BY_BANK_CARD = "Enter <your card number> <dest card number> <amount>"
    ENTER_USER_ID = "Enter user id"
    NO_USER_WITH_ID = "Cannot find user with this id"

    FAVORITE_USERS_LIST_ADD_SUCCESS = "Successfuly added user to favorite list"
    FAVORITE_USERS_LIST_ADD_FAIL = "Failed to add user to favorite list with this id. They are already on the list"
    FAVORITE_USERS_LIST_REMOVE_SUCCESS = "Successfuly removed user from favorite list"
    FAVORITE_USERS_LIST_REMOVE_FAIL = "Failed to remove user from favorite list. They are not on the list"

    ENTER_BANK_CARD_NUMBER_WITH_DATE_RANGE = "Enter Bank Account Card number [date from [date to]]:"
    ENTER_BANK_ACCOUNT_NUMBER_WITH_DATE_RANGE = "Enter Bank Account number [date from [date to]]:"

    PHONE_ALREADY_EXISTS = "Such phone already in use"
    PHONE_ALREADY_EXISTS_TRY_AGAIN = "Such phone already in use, enter another phone number:"
    ENTER_NEW_PASSWORD = "Enter new password"

    def __str__(self):
        return self.value


def build_person_info_message(person: Person):
    return (
        "Information:\n\n"
        f"Id: {person.telegram_chat_id}\n"
        f"Name: {person.name}\n"
        f"Phone: {person.phone}\n"
        f"Date of birth: {person.date_of_birth}\n"
    )


def build_help_message(link: str):
    return (
        "You can save your date using following commands:\n\n"
        "/fill_interactive - start filling in the information interactively (/skip - to skip a question)\n"
        "/cancel - stop conversation\n"
        "/set_phone - enter your phone number\n"
        "/set_name - enter your name\n"
        "/set_date_of_birth - enter the date of birth\n"
        "/set_password - enter new password (for API usage)\n"
        "\nTo see saved information use:\n"
        "/me - show your saved information\n"
        f"follow the link: {link}"
        "\nAccount information and operations:\n"
        "/show_account_balance - show current account balance\n"
        "/show_card_balance - show current card balance\n"
        "/transfer_by_card - transfer amount from card to card\n"
        "/transfer_by_card_with_postcard - transfer amount from card to card with postcard\n"
        "/show_person_cards - show person account and cards by ID\n"
        "\nFavorite users list:\n"
        "/show_favorite_users_list - show favorite users (their names and IDs)\n"
        "/add_to_favorite_users_list - add to favorite users by ID\n"
        "/remove_from_favorite_users_list - remove from favorite users by ID\n"
        "\nStatements:\n"
        "/show_statements_by_card - to see your card statements\n"
        "/show_statements_by_account - to see your account statements\n"
        "/show_transfer_relative_names - to see transfer relative names\n"
        "/show_new_transactions - to see new transactions\n"
    )
