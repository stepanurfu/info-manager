import functools
from enum import Enum, auto

from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, ConversationHandler, Filters, MessageHandler

from .response_messages import PersonInfoResponseFormats


class BotState(Enum):
    PHONE = auto()
    NAME = auto()
    DATE_OF_BIRTH = auto()
    ME = auto()
    HELP = auto()
    BANK_ACCOUNT = auto()
    BANK_CARD = auto()

    TRANSFER = auto()
    FAVORITE_LIST = auto()
    STATEMENTS = auto()

    PASSWORD = auto()


def if_have_message(func):
    @functools.wraps(func)
    def wrapper_decorator(update: Update, context: CallbackContext):
        if update.message is None:
            return
        value = func(update, context)
        return value

    return wrapper_decorator


@if_have_message
def cancel(update: Update, _):
    update.message.reply_text(PersonInfoResponseFormats.CANCEL)
    return ConversationHandler.END


def create_conversation_handler(command_name, cmd_entry, state_handlers, allow_document=False):
    states = {}
    if allow_document:
        filter = (Filters.document | Filters.text) & ~Filters.command
    else:
        filter = Filters.text & ~Filters.command
    for state, handler in state_handlers.items():
        states[state] = [
            MessageHandler(filter, handler),
        ]
    return ConversationHandler(
        entry_points=[CommandHandler(command_name, cmd_entry)],
        states=states,
        fallbacks=[CommandHandler("cancel", cancel)],
    )
