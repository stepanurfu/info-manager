import telegram
from django.conf import settings
from telegram.ext import Dispatcher, Updater

from .handlers.bank_account_handlers import (
    bank_account_balance_conversation_handler,
    bank_card_balance_conversation_handler,
    show_new_transactions_handler,
    show_person_cards_conversation_handler,
    show_statements_by_account_conversation_handler,
    show_statements_by_card_conversation_handler,
    show_transfer_relative_names_handler,
    transfer_by_card_conversation_handler,
    transfer_by_card_with_postcard_conversation_handler,
)
from .handlers.favorite_users_list_handlers import (
    add_to_favorite_users_conversation_handler,
    remove_from_favorite_users_list_conversation_handler,
    show_favorite_users_list_handler,
)
from .handlers.person_info_handlers import (
    date_of_birth_conversation_handler,
    error,
    full_conversation_handler,
    help_handler,
    me_handler,
    name_conversation_handler,
    password_conversation_handler,
    phone_conversation_handler,
    start_handler,
)


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


def add_handlers(dispatcher):
    dispatcher.add_handler(start_handler)
    dispatcher.add_handler(help_handler)
    dispatcher.add_handler(me_handler)
    dispatcher.add_handler(full_conversation_handler)
    dispatcher.add_handler(phone_conversation_handler)
    dispatcher.add_handler(name_conversation_handler)
    dispatcher.add_handler(date_of_birth_conversation_handler)
    dispatcher.add_handler(bank_account_balance_conversation_handler)
    dispatcher.add_handler(bank_card_balance_conversation_handler)
    dispatcher.add_handler(password_conversation_handler)

    dispatcher.add_handler(transfer_by_card_conversation_handler)
    dispatcher.add_handler(transfer_by_card_with_postcard_conversation_handler)
    dispatcher.add_handler(show_person_cards_conversation_handler)
    dispatcher.add_handler(show_favorite_users_list_handler)
    dispatcher.add_handler(add_to_favorite_users_conversation_handler)
    dispatcher.add_handler(remove_from_favorite_users_list_conversation_handler)

    dispatcher.add_handler(show_statements_by_card_conversation_handler)
    dispatcher.add_handler(show_statements_by_account_conversation_handler)
    dispatcher.add_handler(show_transfer_relative_names_handler)
    dispatcher.add_handler(show_new_transactions_handler)

    dispatcher.add_error_handler(error)
    return dispatcher


class TelegramBotWebhook(metaclass=Singleton):
    def __init__(self, token):
        self.bot = telegram.Bot(token=token)
        self.n_workers = 1 if settings.DEBUG else 4
        self.dispatcher = add_handlers(
            Dispatcher(self.bot, update_queue=None, workers=self.n_workers, use_context=True)
        )

    def process(self, update_json):
        update = telegram.Update.de_json(update_json, self.bot)
        self.dispatcher.process_update(update)


class TelegramBotPolling(metaclass=Singleton):
    def __init__(self, token):
        self.token = token
        self.bot = telegram.Bot(token=token)
        self.updater = Updater(token)
        self.dispatcher = add_handlers(self.updater.dispatcher)

    def start(self):
        self._started = True
        self.updater.start_polling()
        self.updater.idle()

    def stop(self):
        self.updater.stop()


telegram_bot_webhook = TelegramBotWebhook(settings.TOKEN)
