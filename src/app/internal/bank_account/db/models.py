from django.db import models

from app.internal.common.models import CommonModel
from app.internal.currency.db.models import Currency
from app.internal.person.db.models import Person


class BankAccount(CommonModel):
    number = models.CharField(max_length=20, unique=True)
    currency = models.ForeignKey(Currency, on_delete=models.SET_NULL, null=True)
    customers = models.ManyToManyField(Person, related_name="bank_accounts")
    closed_at = models.DateTimeField(default=None, blank=True, null=True)

    def __str__(self):
        return f"{self.number}"

    class Meta:
        verbose_name = "Bank Account"
        verbose_name_plural = "Bank Accounts"
