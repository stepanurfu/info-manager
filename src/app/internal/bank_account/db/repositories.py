import decimal
from typing import List, Optional

from django.db.models import Q, Sum

from app.internal.bank_card.db.models import BankCard
from app.internal.common.exceptions import AlreadyExistException, NotFoundException
from app.internal.currency.db.models import Currency
from app.internal.person.db.models import Person

from ..db.models import BankAccount
from ..domain.entities import BankAccountIn


class IBankAccountRepository:
    def get_bank_account_by_id(self, bank_account_id: int) -> BankAccount:
        ...

    def get_bank_accounts(self) -> list[BankAccount]:
        ...

    def add_bank_account(self, bank_account_data: BankAccountIn) -> BankAccount:
        ...

    def get_bank_account_by_card_number(person: Person, bank_card_number: str) -> BankAccount:
        ...

    def get_bank_account_by_number(person: Person, bank_account_number: str) -> BankAccount:
        ...

    def get_bank_account_balance(bank_account: BankAccount) -> decimal.Decimal:
        ...


class BankAccountRepository(IBankAccountRepository):
    def get_bank_account_by_id(self, bank_account_id: int) -> Optional[BankAccount]:
        bank_account: Optional[BankAccount] = BankAccount.objects.filter(pk=bank_account_id).first()
        return bank_account

    def get_bank_accounts(self) -> List[BankAccount]:
        return [BankAccount.objects.all()]

    def add_bank_account(self, bank_account_data: BankAccountIn) -> BankAccount:
        bank_account_data_dict = bank_account_data.dict()
        currency_id = bank_account_data_dict.pop("currency", 1)
        customers = bank_account_data_dict.pop("customers", [])

        currency = Currency.objects.filter(pk=currency_id).first()
        if currency is None:
            raise NotFoundException("Currency", currency_id)
        if BankAccount.objects.filter(number=bank_account_data.number).exists():
            raise AlreadyExistException("BankAccount", "number")
        bank_account: BankAccount = BankAccount.objects.create(**bank_account_data_dict, currency=currency)
        for person in Person.objects.filter(pk__in=customers):
            bank_account.customers.add(person)
        return bank_account

    def get_bank_account_by_card_number(self, person: Person, bank_card_number: str) -> BankAccount:
        if (
            BankAccount.objects.prefetch_related("bank_cards")
            .filter(Q(customers=person) & Q(bank_cards__number=bank_card_number))
            .exists()
        ):
            return BankCard.objects.filter(number=bank_card_number).first().bank_account
        return None

    def get_bank_account_by_number(self, person: Person, bank_account_number: str) -> BankAccount:
        return person.bank_accounts.filter(number=bank_account_number).first()

    def get_bank_account_balance(self, bank_account: BankAccount) -> decimal.Decimal:
        balance = bank_account.bank_cards.aggregate(Sum("balance"))["balance__sum"]
        return balance
