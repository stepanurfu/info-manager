from typing import List

from app.internal.common.exceptions import NotFoundException

from ..db.repositories import IBankAccountRepository
from .entities import BankAccountIn, BankAccountOut


class BankAccountService:
    def __init__(self, bank_account_repo: IBankAccountRepository):
        self._bank_account_repo = bank_account_repo

    def get_bank_account_by_id(self, bank_account_id: int) -> BankAccountOut:
        bank_account = self._bank_account_repo.get_bank_account_by_id(bank_account_id=bank_account_id)
        if bank_account is None:
            raise NotFoundException("BankAccount", bank_account_id)
        return BankAccountOut.from_orm(bank_account)

    def get_bank_accounts(self) -> List[BankAccountOut]:
        bank_accounts = self._bank_account_repo.get_bank_accounts()
        return [BankAccountOut.from_orm(p) for p in bank_accounts]

    def add_bank_account(self, bank_account_data: BankAccountIn) -> BankAccountOut:
        return self._bank_account_repo.add_bank_account(bank_account_data)
