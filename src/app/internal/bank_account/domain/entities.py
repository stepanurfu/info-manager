from typing import List

from ninja import Schema
from pydantic import Field

from app.internal.common.schemas import PkSchema


class BankAccountSchema(Schema):
    number: str = Field(max_length=20)


class BankAccountOut(BankAccountSchema):
    id: int
    currency: PkSchema
    customers: List[PkSchema]

    class Config:
        fields = {"id": "pk"}


class BankAccountIn(BankAccountSchema):
    currency: int
    customers: List[int]
