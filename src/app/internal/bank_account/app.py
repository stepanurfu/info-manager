from ninja import NinjaAPI

from app.internal.authentication.http_jwt_auth import HTTJWTAuth

from .db.repositories import BankAccountRepository
from .domain.services import BankAccountService
from .presentation.handlers import BankAccountHandlers
from .presentation.routers import add_bank_account_router


def get_api():
    api = NinjaAPI(
        title="DT.EDU.BACKEND",
        version="1.0.0",
        auth=[HTTJWTAuth()],
    )
    api = configure_bank_account_api(api)
    return api


def configure_bank_account_api(api: NinjaAPI):
    bank_account_repo = BankAccountRepository()
    bank_account_service = BankAccountService(bank_account_repo=bank_account_repo)
    bank_account_handler = BankAccountHandlers(bank_account_service=bank_account_service)
    add_bank_account_router(api, bank_account_handler)
    return api
