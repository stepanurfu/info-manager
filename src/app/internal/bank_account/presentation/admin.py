from django.contrib import admin

from ..db.models import BankAccount


@admin.register(BankAccount)
class BankAccountAdmin(admin.ModelAdmin):
    fields = ("number", "currency", "customers")
    filter_horizontal = ("customers",)
