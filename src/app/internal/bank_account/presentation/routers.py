from ninja import NinjaAPI, Router

from app.internal.common.responses import NotFoundResponse

from ..domain.entities import BankAccountOut
from ..presentation.handlers import BankAccountHandlers


def get_bank_account_router(bank_account_handlers: BankAccountHandlers):
    router = Router(tags=["bank_accounts"])

    router.add_api_operation(
        "/{int:bank_account_id}",
        ["GET"],
        bank_account_handlers.get_bank_account_by_id,
        response={200: BankAccountOut, 404: NotFoundResponse},
    )

    router.add_api_operation(
        "",
        ["POST"],
        bank_account_handlers.add_bank_account,
        response={200: BankAccountOut, 404: NotFoundResponse},
    )

    return router


def add_bank_account_router(api: NinjaAPI, bank_account_handlers: BankAccountHandlers):
    persons_handler = get_bank_account_router(bank_account_handlers)
    api.add_router("/bank_accounts", persons_handler)
