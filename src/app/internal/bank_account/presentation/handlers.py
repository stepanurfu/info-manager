from typing import List

from ninja import Body, Path

from ..domain.entities import BankAccountIn, BankAccountOut
from ..domain.services import BankAccountService


class BankAccountHandlers:
    def __init__(self, bank_account_service: BankAccountService):
        self._bank_account_service = bank_account_service

    def get_bank_account_by_id(self, request, bank_account_id: int = Path(...)) -> BankAccountOut:
        bank_account = self._bank_account_service.get_bank_account_by_id(bank_account_id=bank_account_id)
        return bank_account

    def get_bank_accounts(self, request) -> List[BankAccountOut]:
        return self._bank_account_service.get_bank_accounts()

    def add_bank_account(self, request, bank_account_data: BankAccountIn = Body(...)) -> BankAccountOut:
        return self._bank_account_service.add_bank_account(bank_account_data)
