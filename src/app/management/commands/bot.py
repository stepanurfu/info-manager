from django.conf import settings
from django.core.management.base import BaseCommand

from app.internal.bot.bot import TelegramBotPolling


class Command(BaseCommand):
    help = "Start Telegram Bot Polling"

    def handle(self, *args, **options):
        bot = TelegramBotPolling(settings.TOKEN)
        bot.start()
