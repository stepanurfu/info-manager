all: down build up makemigrations migrate createsuperuser

run: down build up


run_migrate:
	python src/manage.py migrate $(if $m, api $m,)

run_makemigrations:
	python src/manage.py makemigrations
	sudo chown -R ${USER} src/app/migrations/

run_createsuperuser:
	python src/manage.py createsuperuser

run_collectstatic:
	python src/manage.py collectstatic --no-input

run_dev:
	python src/manage.py runserver localhost:8000

run_command:
	python src/manage.py ${c}

run_shell:
	python src/manage.py shell

run_debug:
	python src/manage.py debug

run_piplock:
	pipenv install
	sudo chown -R ${USER} Pipfile.lock

run_lint:
	isort .
	flake8 --config setup.cfg
	black --config pyproject.toml .

check_lint:
	isort --check --diff .
	flake8 --config setup.cfg
	black --check --config pyproject.toml .

run_bot:
	python src/manage.py bot

run_server:
	python src/manage.py runserver 0.0.0.0:8000

build_old:
	docker build . -t django_bank_kachusov:4

run_test_all: run_test_unit run_test_integration run_test_smoke

run_test_unit:
	cd src && pytest test/unit

run_test_integration:
	cd src && pytest test/integration

run_test_smoke:
	cd src && pytest test/smoke

build:
	docker-compose build 

up:
	docker-compose up -d

makemigrations:
	docker-compose exec web python src/manage.py makemigrations

# migrate:
# 	docker-compose exec web python src/manage.py migrate $(if $m, api $m,)

createsuperuser:
	docker-compose exec web python src/manage.py createsuperuser

collectstatic:
	docker-compose exec web python src/manage.py collectstatic --no-input

dev:
	docker-compose exec web python src/manage.py runserver localhost:8000

command:
	docker-compose exec web python src/manage.py ${c}

shell:
	docker-compose exec web python src/manage.py shell

debug:
	docker-compose exec web python src/manage.py debug

piplock:
	docker-compose exec web pipenv install && sudo chown -R ${USER} Pipfile.lock

down:
	docker-compose down

push:
	docker push ${IMAGE_APP}

pull:
	docker pull ${IMAGE_APP}

test:
	echo "No tests"

lint:
	docker run --rm ${IMAGE_APP} make check_lint

migrate:
	docker-compose run --rm web bash -c "/wait && python ./src/manage.py migrate"

test_all:
	docker-compose run --rm web bash -c "/wait && cd src && pytest"

test_unit:
	docker-compose exec web bash -c "cd src && pytest test/unit"

test_integration:
	docker-compose exec web bash -c "cd src && pytest test/integration"

test_smoke:
	docker-compose exec web bash -c "cd src && pytest test/smoke"