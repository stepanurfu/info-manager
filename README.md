# Telegram bot to store user information

## Usage
#### 1. Go to the project folder and create a `src/config/.env` file and fill in the following fields:
```
POSTGRES_HOST=
POSTGRES_PORT=
POSTGRES_DB=
POSTGRES_USER=
POSTGRES_PASSWORD=
TELEGRAM_TOKEN=
SECRET_KEY=
BASE_URL=
```
- To generate a SECRET_KEY you can use
```
from django.core.management.utils import get_random_secret_key  
get_random_secret_key()
```
#### Example of a completed `src/config/.env` file:<br />
```
POSTGRES_HOST=localhost
POSTGRES_PORT=5432
POSTGRES_DB=django_test_db
POSTGRES_USER=postgres
POSTGRES_PASSWORD=postgres
TELEGRAM_TOKEN=2004739603:AAH71ghhJWy91iLTFAbrBzvJkzoj1OArjfA
SECRET_KEY="ptd8672i#yobzd(&!ras5k@16zyziz=$5kgt87ly5-v#j)3s&v"
BASE_URL="http://192.168.1.7:8000"
```

#### 2. Go to the project folder and execute the following commands
- `make piplock`
- `make makemigrations`
- `make migrate`
- `make createsuperuser`

#### 3. Use the `make run_bot` command to start the Telegram bot
#### 4. To start the server, use `make run_server` or `make dev`
##### To start the telegram bot and server at the same time, you can run steps 3 and 4 in parallel in two terminals
#### 5. Go to the admin page `endpoint/admin/`

<br />

## Interacting through a telegram bot
Entering a phone number is compulsory
- `/fill_interactive` - Start filling in the information interactively (`/skip` - to skip a question)
- `/cancel` - stop conversation
- `/set_phone` - enter your phone number
- `/set_name` - enter your name
- `/set_email` - enter your email
- `/set_date_of_birth` - enter the date of birth
- `/set_description` - enter a short description about yourself

To see saved information use:
- /me - show the saved information
- follow the link: http(s)://<HOST>:<PORT>/api/<chat_id>/me

To see current balance
- `/show_balance` - show current balance (You will be asked to enter your bank account/card number)


## Run with docker

Note:
 - Database data will be saved in `../data_django_bank_kachusov` folder
 - Image `django_bank_kachusov:1` will be built
 - 3 containers (for webserver, telegram bot and database) will be created

#### 1. modify `src/config/.env` file, so `POSTGRES_HOST` has value `db`
```
POSTGRES_HOST=db
```

#### 2. Build image and start server, telegram bot and postgresql in docker
Run the following command:
```
make run
```

Then register superuser
```
make createsuperuser
```

When the database local data is formed and it starts up fast enough, you can run 
```
make
```


## Run during deployment

If the database is initialized for the first time, run the following commands while in the virtual machine to register the superuser

#### 1. Determine the container ID with the server using:
```
docker ps
```

#### 2. Register the superuser
```
docker exec -it <container id> /bin/bash
python src/manage.py createsuperuser
exit
```

## Set Webhook
Follow this link:
```
https://api.telegram.org/bot<telegram_token>/setWebhook?url=https://<host>:<port>/<telegram_token>/
```

## Performance Test
 - Choose folder <folder> to save test logs
 - Copy load.yaml in <folder>
 - Get api token from https://overload.yandex.net/
 - Create file tokem.txt in <folder>
 - Run command:
```
docker run -v <folder>:/var/loadtest --net host -it direvius/yandex-tank
```
 - See results at https://overload.yandex.net/



## Authentication
| Action        | Method           | Endpoint  |
| ------------- |:-------------| :--------|
| Log in | POST | /login | 
| Refresh tokens | POST | /refresh |
| Get information about user | GET | /api/me |
| Get bank account and card information | GET | /api/bank_cards |

### POST /login
```
{
    "phone": "11111111111",
    "password": "juan"
}
```

### POST /refresh
```
{
    "refresh_token": "eyJ0eXAiOiJKV1...vQ"
}
```

### For the following endpoints you must specify Authorization header
```
Authorization: Bearer eyJ0eXAiO....-vA
```
- ```GET /api/me```
- ```GET /api/bank_cards```
